package lambda;


import classes.Human;

import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;


public class LambdaDemo {
    public static Function<String, Integer> lenghtStr = String::length;

    public static Function<String, Character> takeFirst = (str) -> {
        if (str == null || str.length() == 0) {
            return null;
        }
        return str.charAt(0);
    };

    public static Predicate<String> haveEnter = str -> str.indexOf(' ') == -1;

    public static Function<String, Integer> amountWords = (str) -> {
        if (str == null || str.length() == 0) {
            return 0;
        }
        int word = 0;
        int answer = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) != ',') {
                word++;
            }
            if (str.charAt(i) == ',') {
                if (word != 0) {
                    answer++;
                }
                word = 0;
            }
        }
        if (word != 0) {
            return answer + 1;
        }
        return answer;
    };

    public static Function<Human, Integer> getAgeHuman = Human::getAge;

    public static Function<Human, String> getFIO = (h) -> h
            .getName()
            .concat(" ")
            .concat(h.getSurname())
            .concat(" ")
            .concat(h.getPatronymic());

    public static BiPredicate<Human, Human> sameSurname = (h, h1) -> h.getSurname().equals(h1.getSurname());

    public static UnaryOperator<Human> plasYear = (h) -> new Human(h.getName(), h.getSurname(), h.getPatronymic(), h.getAge() + 1, h.getG());

    public static bool4Arg<Human, Human, Human, Integer> yongerAge = (h1, h2, h3, mAge) ->
            h1.getAge() < mAge && h2.getAge() < mAge && h3.getAge() < mAge;


    @FunctionalInterface
    public interface bool4Arg<A, B, C, D> {
        boolean check(A h, B h1, C h2, D mAge);
    }

}