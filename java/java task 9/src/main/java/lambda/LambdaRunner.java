package lambda;

import classes.Human;

import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class LambdaRunner {


    public static <T, R> R runner1(Function<T, R> a, T b) {
        return a.apply(b);
    }

    public static <T> boolean runner2(BiPredicate<T, T> a, T b, T b1) {
        {
            return a.test(b, b1);
        }
    }

    public static <T> boolean runner3(Predicate<T> a, T b) {
        return a.test(b);
    }

    public static <T> T runner4(UnaryOperator<T> a, T b) {
        return a.apply(b);
    }

    public static <T, R> boolean runner5(LambdaDemo.bool4Arg<T, T, T, R> a, T b1, T b2, T b3, R b4 ) {
        return a.check(b1, b2, b3, b4);
    }

}
