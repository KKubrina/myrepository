package classes;

import java.util.Objects;

public class Student extends Human {
    String facultyName;
    String university;
    String speciality;

    public Student(String name, String surname, String patronymic, int age, Gender g, String facultyName, String university, String speciality) {
        super(name, surname, patronymic, age, g);
        this.facultyName = facultyName;
        this.university = university;
        this.speciality = speciality;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        if (!super.equals(o)) return false;
        Student student = (Student) o;
        return Objects.equals(getFacultyName(), student.getFacultyName()) &&
                Objects.equals(getUniversity(), student.getUniversity()) &&
                Objects.equals(getSpeciality(), student.getSpeciality());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getFacultyName(), getUniversity(), getSpeciality());
    }
}
