package lambda.test;

import classes.Human;
import lambda.LambdaDemo;
import lambda.LambdaRunner;
import org.junit.Test;

import static classes.Gender.Men;
import static classes.Gender.Women;
import static org.junit.Assert.*;

public class LambdaRunnerTest {


    @Test
    public void lenghtStr() {
        int a = LambdaRunner.runner1(LambdaDemo.lenghtStr, "12345");
        assertEquals(5, a);
    }

    @Test
    public void lenghtStr1() {
        int a = LambdaRunner.runner1(LambdaDemo.lenghtStr, "");
        assertEquals(0, a);
    }

    @Test
    public void amountWords() {
        int a = LambdaRunner.runner1(LambdaDemo.amountWords, "12,34,5");
        assertEquals(3, a);
    }

    @Test
    public void amountWords1() {
        int a = LambdaRunner.runner1(LambdaDemo.amountWords, ",34,");
        assertEquals(1, a);
    }

    @Test
    public void amountWords2() {
        int a = LambdaRunner.runner1(LambdaDemo.amountWords, ",,,");
        assertEquals(0, a);
    }

    @Test
    public void takeFirst() {
        char a = LambdaRunner.runner1(LambdaDemo.takeFirst, "abc");
        assertEquals('a', a);

    }

    @Test
    public void takeFirst1() {
        assertNull(LambdaRunner.runner1(LambdaDemo.takeFirst, ""));

    }

    @Test
    public void getAgeHuman() {
        Human sasha = new Human("Александр", "Иванов", "Олегович", 24, Men);
        int a = LambdaRunner.runner1(LambdaDemo.getAgeHuman, sasha);
        assertEquals(24, a);

    }

    @Test
    public void getFIO() {
        Human sasha = new Human("Александр", "Иванов", "Олегович", 24, Men);
        String a = LambdaRunner.runner1(LambdaDemo.getFIO, sasha);
        assertEquals("Александр Иванов Олегович", a);

    }

    @Test
    public void haveEnter() {
        boolean a = LambdaRunner.runner3(LambdaDemo.haveEnter, "1 23 45");
        assertFalse(a);
    }

    @Test
    public void haveEnter1() {
        boolean a = LambdaRunner.runner3(LambdaDemo.haveEnter, "12345");
        assertTrue(a);
    }

    @Test
    public void sameSurname() {
        Human sasha = new Human("Александр", "Иванов", "Олегович", 44, Men);
        Human pasha = new Human("Павел", "Иванов", "Александрович", 24, Men);
        assertTrue(LambdaRunner.runner2(LambdaDemo.sameSurname, sasha, pasha));
    }

    @Test
    public void sameSurname1() {
        Human sasha = new Human("Александр", "Иванов", "Олегович", 44, Men);
        Human pasha = new Human("Павел", "Петров", "Александрович", 24, Men);
        assertFalse(LambdaRunner.runner2(LambdaDemo.sameSurname, sasha, pasha));
    }

    @Test
    public void plasYear() {
        Human pasha = new Human("Павел", "Иванов", "Александрович", 24, Men);
        Human h = LambdaRunner.runner4(LambdaDemo.plasYear, pasha);
        assertEquals(new Human("Павел", "Иванов", "Александрович", 25, Men), h);
    }

    @Test
    public void yongerAge() {
        Human sasha = new Human("Александр", "Иванов", "Олегович", 44, Men);
        Human pasha = new Human("Павел", "Иванов", "Александрович", 24, Men);
        Human masha = new Human("Мария", "Иванова", "Алексеевна", 45, Women);
        int max = 25;
        boolean a = LambdaRunner.runner5(LambdaDemo.yongerAge, sasha, pasha, masha, max);
        assertFalse(a);
    }

    @Test
    public void yongerAge1() {
        Human sasha = new Human("Александр", "Иванов", "Олегович", 24, Men);
        Human pasha = new Human("Павел", "Иванов", "Александрович", 24, Men);
        Human masha = new Human("Мария", "Иванова", "Алексеевна", 25, Women);
        int max = 25;
        boolean a = LambdaRunner.runner5(LambdaDemo.yongerAge, sasha, pasha, masha, max);
        assertFalse(a);
    }

    @Test
    public void yongerAge2() {
        Human sasha = new Human("Александр", "Иванов", "Олегович", 24, Men);
        Human pasha = new Human("Павел", "Иванов", "Александрович", 24, Men);
        Human masha = new Human("Мария", "Иванова", "Алексеевна", 12, Women);
        int max = 25;
        boolean a = LambdaRunner.runner5(LambdaDemo.yongerAge, sasha, pasha, masha, max);
        assertTrue(a);
    }
}