package service.test;

import org.junit.Assert;
import org.junit.Test;
import service.FileFilterExtends;
import service.Service;

import java.io.*;

import static org.junit.Assert.assertArrayEquals;


public class ServiceTest {

    @Test
    public void method1() throws IOException {
        int [] array = new int [] {-1, 2, -3, 4, 55, 6};
        DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("test.txt")));
        Service.method1(out, array);
        DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream("test.txt")));
        int [] array1 = Service.method1a(in, array.length);
        assertArrayEquals(array, array1);
    }

    @Test
    public void method2() throws IOException {
        int [] array = new int [] {-1, 2, -3, 4, 55, 6};
        BufferedWriter out = new BufferedWriter(new FileWriter("test.txt"));
        Service.method2(out, array);
        BufferedReader in = new BufferedReader(new FileReader("test.txt"));
        int [] array1 = Service.method2a(in, array.length);
        assertArrayEquals(array, array1);
    }



    @Test
    public void method3() throws IOException {
        int [] array = new int [] {-1, 2, -3, 4, 55, -66};
        DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("test.txt")));
        Service.method1(out, array);
        RandomAccessFile f = new RandomAccessFile("test.txt", "r");
        int [] array1;
        array1 = Service.method3(f, 3);
        int [] array2 = new int [] { 4, 55, -66};
        assertArrayEquals(array2, array1);
    }

    @Test
    public void method3a() throws IOException {
        int [] array = new int [] {-1, 2, -3, 4, 55, -66};
        DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("test.txt")));
        Service.method1(out, array);
        RandomAccessFile f = new RandomAccessFile("test.txt", "r");
        int [] array1;
        array1 = Service.method3(f, 6);
        int [] array2 = new int [] {};
        assertArrayEquals(array2, array1);
    }

    @Test
    public void method3b() throws IOException {
        int [] array = new int [] {-1, 2, -3, 4, 55, -66};
        DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("test.txt")));
        Service.method1(out, array);
        RandomAccessFile f = new RandomAccessFile("test.txt", "r");
        try {
            Service.method3(f, 7);
            Assert.fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException thrown) {
            Assert.assertNotEquals("", thrown.getMessage());
        }
    }



    @Test
    public void method4(){
        FileFilterExtends filter2 = new FileFilterExtends(".docx");
        File file2 = new File("TestNew");
        String[] arrayFilter1 =  Service.method4(file2, filter2);
        String[] arrayFilter = new String[] {"0.docx", "4.docx"};
        assertArrayEquals(arrayFilter, arrayFilter1);
    }

    @Test
    public void method4a() {
        FileFilterExtends filter1 = new FileFilterExtends(".txt");
        File file2 = new File("TestNew");
        String[] arrayFilter1 =  Service.method4(file2, filter1);
        String[] arrayFilter = new String[] {"1.txt", "2.txt", "3.txt"};
        assertArrayEquals(arrayFilter, arrayFilter1);
    }

    @Test (expected = IllegalArgumentException.class)
    public void method4b() {
        FileFilterExtends filter1 = new FileFilterExtends(".txt");
        File file2 = new File("Test.txt");
        String[] arrayFilter1 =  Service.method4(file2, filter1);
    }

    @Test (expected = IllegalArgumentException.class)
    public void method4c() throws IOException {
        FileFilterExtends filter1 = new FileFilterExtends(".txt");
        File file2 = new File("Papka");
        String[] arrayFilter1 =  Service.method4(file2, filter1);
    }



}