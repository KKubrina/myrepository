package service;

import classes.House;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class ServiceHouse {

    public static void method5(ObjectOutput out, House house) throws IOException {
        house.serialize(out);
        out.flush();
    }

    public static House method6(ObjectInput in) throws IOException, ClassNotFoundException {
        return (House) in.readObject();
    }

    public static String method7(House house) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(house);
    }

    public static House method8(String jsonString) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonString, House.class);
    }
}
