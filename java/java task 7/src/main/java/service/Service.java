package service;


import java.io.*;
import java.util.Scanner;


public class Service {


    public static void method1(DataOutputStream out, int[] array) throws IOException {
        for (int value : array) {
            out.writeInt(value);
        }
        out.flush();
    }

    public static int[] method1a(DataInputStream in, int size) throws IOException {
        int[] arrayAnswer = new int[size];
        for (int i = 0; i < size; i++) {
            arrayAnswer[i] = in.readInt();
        }
        return arrayAnswer;
    }

    public static void method2(Writer out, int[] array) throws IOException {
        PrintWriter prWr = new PrintWriter(out);
        for (int value : array) {
            prWr.print(value);
            prWr.print(' ');
        }
        out.flush();
    }

    public static int[] method2a(Reader in, int size) {
        int[] arrayAnswer = new int[size];
        Scanner scanner = new Scanner(in);
        for (int i = 0; i < size; i++) {
            arrayAnswer[i] = scanner.nextInt();
        }
        return arrayAnswer;
    }

    public static int[] method3(RandomAccessFile rAF, int pointer) throws IOException {
        try {
            rAF.seek(pointer * 4);
        } catch (IOException e) {
            System.err.println("Pointer error" + e);
        }
        if (pointer <= rAF.length() / 4) {
            int[] array = new int[(int) (rAF.length() - rAF.getFilePointer()) / 4];
            for (int i = 0; i < array.length; i++) {
                array[i] = rAF.readInt();
            }
            rAF.close();
            return array;
        } else {
            throw new IllegalArgumentException("Pointer went beyond");
        }

    }


    public static String[] method4(File file, FilenameFilter filter) {
        if (file.exists()) {
            if (file.isDirectory()) {
                return file.list(filter);
            } else {
                throw new IllegalArgumentException("File is not directory");
            }
        } else {
            throw new IllegalArgumentException("File does not exit");
        }

    }
}


