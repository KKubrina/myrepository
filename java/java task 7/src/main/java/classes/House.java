package classes;

import java.io.IOException;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class House implements Serializable {
    String cadastralNumber;
    String adress;
    Person mainOfHouse;
    ArrayList<Flat> flats;

    public void serialize(ObjectOutput out) throws IOException {
        out.writeObject(this);
    }

    public House() {
        super();
    }


    public void setCadastralNumber(String cadastralNumber) {
        this.cadastralNumber = cadastralNumber;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setMainOfHouse(Person mainOfHouse) {
        this.mainOfHouse = mainOfHouse;
    }

    public void setFlats(ArrayList<Flat> flats) {
        this.flats = flats;
    }

    public House(String cadastralNumber, String adress, Person mainOfHouse, ArrayList<Flat> flats) {
        this.cadastralNumber = cadastralNumber;
        this.adress = adress;
        this.mainOfHouse = mainOfHouse;
        this.flats = flats;
    }

    public String getCadastralNumber() {
        return cadastralNumber;
    }

    public String getAdress() {
        return adress;
    }

    public Person getMainOfHouse() {
        return mainOfHouse;
    }

    public ArrayList<Flat> getFlats() {
        return flats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof House)) return false;
        House house = (House) o;
        return Objects.equals(getCadastralNumber(), house.getCadastralNumber()) &&
                Objects.equals(getAdress(), house.getAdress()) &&
                Objects.equals(getMainOfHouse(), house.getMainOfHouse()) &&
                Objects.equals(getFlats(), house.getFlats());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCadastralNumber(), getAdress(), getMainOfHouse(), getFlats());
    }
}
