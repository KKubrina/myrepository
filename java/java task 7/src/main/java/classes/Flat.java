package classes;

import java.io.IOException;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class Flat implements Serializable {
    int number;
    double area;
    ArrayList<Person> tenans;

    public Flat() {
        super();
    }

    public void serialize(ObjectOutput out) throws IOException {
        out.writeObject(this);
    }

    public Flat(int number, double area, ArrayList<Person> tenans) {
        this.number = number;
        this.area = area;
        this.tenans = tenans;
    }

    public int getNumber() {
        return number;
    }

    public double getArea() {
        return area;
    }

    public ArrayList<Person> getTenans() {
        return tenans;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public void setTenans(ArrayList<Person> tenans) {
        this.tenans = tenans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Flat)) return false;
        Flat flat = (Flat) o;
        return getNumber() == flat.getNumber() &&
                Double.compare(flat.getArea(), getArea()) == 0 &&
                Objects.equals(getTenans(), flat.getTenans());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNumber(), getArea(), getTenans());
    }
}
