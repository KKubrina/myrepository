package classes;

import java.io.*;
import java.util.Objects;

public class Person implements Serializable {
    String name;
    String surname;
    String patronymic;
    String dareOfBirth;

    public void serialize(ObjectOutput out) throws IOException {
        out.writeObject(this);
    }

    public Person() {
        super();

    }

    public Person(String name, String surname, String patronymic, String dareOfBirth) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.dareOfBirth = dareOfBirth;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getDareOfBirth() {
        return dareOfBirth;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setDareOfBirth(String dareOfBirth) {
        this.dareOfBirth = dareOfBirth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name) &&
                Objects.equals(surname, person.surname) &&
                Objects.equals(patronymic, person.patronymic) &&
                Objects.equals(dareOfBirth, person.dareOfBirth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, patronymic, dareOfBirth);
    }
}
