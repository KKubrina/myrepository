package equations;

public class SquareTrinomial {
    private double a;
    private double b;
    private double c;

    public SquareTrinomial(double a, double b, double c) {

        this.setA(a);
        this.b = b;
        this.c = c;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public void setA(double a) {
        if (Double.compare(a, 0.0) == 0){throw new IllegalArgumentException("Нельзя так!");}
        else {this.a = a;}
    }

    public void setB(double b) {
        this.b = b;
    }

    public void setC(double c) {
        this.c = c;
    }

    public double getC() {
        return c;
    }

    public double[] roots (){
        double D = b*b-4*a*c;
        double[] array = {};
        if (D>0){
            array = new double[2];
            array[0] = (-b + Math.sqrt(D))/(2*a);
            array[1] = (-b - Math.sqrt(D))/(2*a);
          ;

        }

        if (Double.compare(D, 0.0) == 0){
            array = new double[1];
            array[0] = -b /(2*a);

        }
        return array;
    }

}
