package equations;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class SquareTrinomialTest {

    private static final double DELTA = 1e-13;
    SquareTrinomial trinomial;

    // a != 1
//    2 roots
//    1 root
//    0 root
//    not a SquareEq
//
    @Test
    public void roots2() {
        trinomial = new SquareTrinomial(6, -4, -2);
        double[] b = trinomial.roots();
        double[] b1 = {1.0, -1.0 / 3};
        assertArrayEquals(b1, b, DELTA);
    }

    @Test
    public void roots1() {
        trinomial = new SquareTrinomial(4, -4, 1);
        double[] b;
        b = trinomial.roots();
        double[] b1 = {1.0 / 2};
        assertArrayEquals(b1, b, DELTA);
    }

    @Test
    public void roots0() {
        trinomial = new SquareTrinomial(5, -4, 1);
        double[] b = trinomial.roots();
        double[] b1 = {};
        assertArrayEquals(b1, b, DELTA);
    }

    @Test(expected = IllegalArgumentException.class)
    public void roots() {
        trinomial = new SquareTrinomial(0, -4, 1);
    }

}
