package second.matr_service;

import second.matrixes.Matrix;

import java.util.Comparator;

public class DeterminantComparator implements Comparator<Matrix> {

    @Override
    public int compare(Matrix m1, Matrix m2) {

        if(m1.determinant() > m2.determinant())
            return 1;
        else if(m1.determinant() < m2.determinant())
            return -1;
        else
            return 0;
    }
}
