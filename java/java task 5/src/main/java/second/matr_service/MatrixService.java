package second.matr_service;

import second.matrixes.IMatrix;
import second.matrixes.Matrix;

import java.util.Arrays;

public class MatrixService <T extends IMatrix>{
    T a;
    static public Matrix[] arrangeMatrices(Matrix[] arrayM){
        Arrays.sort(arrayM, new DeterminantComparator());
        return arrayM;
    }
    static public int arrangeMatrices(IMatrix M){
        if(M.determinant()==0){
        return 0;}
        return -1;
    }
}
