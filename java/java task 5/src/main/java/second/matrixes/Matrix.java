package second.matrixes;


import java.util.Arrays;
import java.util.Objects;

public class Matrix implements IMatrix {
    private int N;
    private double matrix[];
    protected boolean determinantFlag;
    protected double determinant;


    public Matrix(int n){
        this.N = n;
        matrix = new double[n*n];
        determinantFlag = false;
    }

    public Matrix(int n, double ...m) {
        if (m.length < n * n) {
            throw new IllegalArgumentException("Недостаточно элементов в массиве");
        }


        this.N = n;
        this.matrix = Arrays.copyOf(m, n*n);
        determinantFlag = false;

    }

    public Matrix() {
    }

    public int getN() {
        return N;
    }

    public double[] getMatrix() {
        return matrix;
    }

    public void setN(int n) {
        this.N = n;
    }

    public void setMatrix(double[] matrix) {
        this.matrix = matrix;
    }

    public void setMatrixI(double a, int i) {
        this.matrix[i] = a;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Matrix)) return false;
        Matrix matrix = (Matrix) o;
        return N == matrix.N &&
                Arrays.equals(this.matrix, matrix.matrix);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(N);
        result = 31 * result + Arrays.hashCode(matrix);
        return result;
    }


    public double getElement(int s, int c){
    return matrix[N*(s-1)+(c-1)];
    }


    public void setElement(int s, int c, double e){
        matrix[N*(s-1)+(c-1)] = e;
        determinantFlag = false;
    }


    public double determinant(){
    if (!determinantFlag){
       int str=0;
       int znak = 0;
       int i;
       double element, koef, determ=1;
        for(int j = 0; j < N-1; j++){
            i = str;
            while((Double.compare(matrix[N*i+j], 0.0 ) == 0)&&(i<N-1)){
                i+=1;
            } //ищем в столбце не нулевой элемент
            if (Double.compare(matrix[N*i+j], 0.0 ) == 0){ return 0;}
            if(i!=str){
                znak+=1;
                double a = 0;
                for(int d = 0; d < N; d++){
                    a = matrix[N*i+d];
                    matrix[N*i+d] = matrix[str*N+d];
                    matrix[str*N+d] = a;
                }
            } //если он не в диагонале, меняем строки местами
            for(int d=str+1; d<N;d++) {
                if (Double.compare(matrix[N * d + j], 0.0) != 0) {
                    koef = matrix[N * d + j] / matrix[N * i + j];
                    for (int b = 0; b < N; b++) {
                        matrix[N * d + b] = matrix[N * d + b] - matrix[N * str + b] * koef;
                    }
                }
            }
            determ *= matrix[N*str+j];
            str+=1;
        }
        determ *= matrix[N*(N-1)+N-1];
        if (znak%2!=0){
            determ = 0 - determ;
        }
        determinantFlag = true;
        determinant = determ;}

        return determinant;

    }
}
