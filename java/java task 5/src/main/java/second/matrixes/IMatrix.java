package second.matrixes;

public interface IMatrix {
    double getElement(int s, int c);
    void setElement(int s, int c, double e);
    double determinant();
}
