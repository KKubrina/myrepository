package second.matrixes;

public class UpTriangleMatrix extends Matrix implements IMatrix {

    public UpTriangleMatrix(int n) {
        setN(n);
        setMatrix(new double[((n/2)+(n*n)/2)]);
        this.determinantFlag = false;    }

    public UpTriangleMatrix(int n, double m[]) {
        setN(n);
        setMatrix(new double[((n/2)+(n*n)/2)]);
        if (m.length<(n/2)+(n*n)/2){ throw new IllegalArgumentException("Element outside the up triangle"); }
        else {
            int a=0;
            for(int i = 0; i < (n/2)+(n*n)/2; i++){
                  setMatrixI(m[a], i);
                    a+=1;

            }
        }
    }

    @Override
    public void setElement(int s, int c, double e){
        if(s>c){ throw new IllegalArgumentException("Element outside the up triangle"); }
        int step=((s-1) * (s ) / 2);
        if(getN()*(s-1)+(c-1)-step > getN()){ throw new IllegalArgumentException("Element outside the up triangle"); }
        setMatrixI(e, getN()*(s-1)+(c-1)-step);

    }

    @Override
    public double getElement(int s, int c){
        if(s>c){ throw new IllegalArgumentException("Element outside the up triangle"); }
        int step=((s-1) * (s ) / 2);
        if(getN()*(s-1)+(c-1)-step > getN()){ throw new IllegalArgumentException("Element outside the up triangle"); }
        return getMatrix()[getN()*(s-1)+(c-1)-step];
    }

    @Override
    public double determinant(){
        if (!determinantFlag){
            double determ = 1;
            for(int i = 0; i < getN(); i++){
                int step=((i) * (i+1) / 2);
                determ *= getMatrix()[getN()*i+i-step];
            }
            determinant = determ;
        }
        return determinant;

    }

}
