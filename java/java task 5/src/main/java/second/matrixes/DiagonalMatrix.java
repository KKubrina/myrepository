package second.matrixes;

public class DiagonalMatrix extends Matrix {


    public DiagonalMatrix(int n){
        setN(n);
        setMatrix(new double[n]);
        this.determinantFlag = false;
    }

    public DiagonalMatrix(int n, double ...m) {
        setN(n);
        setMatrix(new double[n]);
        if (m.length<n){ throw new IllegalArgumentException("Недостаточно элементов в массиве"); }
        else {
            for(int i = 0; i < n; i++){
                setMatrixI(m[i], i);
            }
        }
        this.determinantFlag = false;
    }


    @Override
    public void setElement(int s, int c, double e){
        if(s>getN()){ throw new IllegalArgumentException("Element outside the diagonal"); }
        setMatrixI(e, s-1);
    }

    @Override
    public double getElement(int s, int c){
        if(s>getN()){ throw new IllegalArgumentException("Element outside the diagonal"); }
        return getMatrix()[s-1];
    }

    @Override
    public double determinant(){
        if (!determinantFlag){
            double determ = 1;
            for(int i = 0; i < getN(); i++){
                determ *= getMatrix()[i];
            }
            determinant = determ;
        }
        return determinant;

    }
}
