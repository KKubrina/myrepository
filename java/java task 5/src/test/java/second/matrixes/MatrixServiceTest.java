package second.matrixes;

import org.junit.Test;
import second.matr_service.DeterminantComparator;
import second.matr_service.MatrixService;
import second.matrixes.Matrix;

import static org.junit.Assert.assertEquals;


public class MatrixServiceTest {

    @Test
    public void arrangeMatrices() {
        Matrix m = new Matrix(2, 1, 2, 3, 4); //определитель -2
        Matrix m1 = new Matrix(2, 1, 2, -3, -4); //опр. 2
        Matrix m2 = new Matrix(2, 1, 2, 1, 2); //опр. 0
        DeterminantComparator dC = new DeterminantComparator();
        Matrix[] arrayMatrix = new Matrix[] {m, m1, m2};
        Matrix[] arrayMatrix1 = new Matrix[] {};
        arrayMatrix1 = MatrixService.arrangeMatrices(arrayMatrix);
        assertEquals(m,arrayMatrix1[0] );
        assertEquals(m2,arrayMatrix1[1] );
        assertEquals(m1,arrayMatrix1[2] );

    }
}