package second.matrixes;

import org.junit.Test;
import second.matrixes.UpTriangleMatrix;

import static org.junit.Assert.assertEquals;


public class UpTriangleMatrixTest {

    @Test
    public void constructorAndDeterminant() {
        UpTriangleMatrix m = new UpTriangleMatrix(2, new double[]{-2.0, 3.0, -4.0});
        double a = m.determinant();
        assertEquals(a, 8,1e-10);
    }

    @Test
    public void setElement() {
        UpTriangleMatrix m = new UpTriangleMatrix(2, new double[]{-2.0, 3.0, -4.0});
        double a = m.determinant();
        assertEquals(a, 8,1e-10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setElement1() {
        UpTriangleMatrix m = new UpTriangleMatrix(2, new double[]{-2.0, 3.0, -4.0});
         m.setElement(2,1,2);
    }
}