package second.matrixes;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class DiagonalMatrixTest {

    @Test(expected = IllegalArgumentException.class)
    public void construct() {
        DiagonalMatrix m = new DiagonalMatrix(2, 2.0);

    }

    @Test
    public void construct1() {
        DiagonalMatrix m = new DiagonalMatrix(2, 2.0, 3.0);
        assertEquals(2, m.getElement(1, 1), 1e-10);
        assertEquals(3, m.getElement(2, 2), 1e-10);
    }

    @Test
    public void setElement() {
        DiagonalMatrix m = new DiagonalMatrix(3, 2.0, 2.5, 2.0);
        m.setElement(1,1,-2);
        assertEquals(-2, m.getElement(1, 1), 1e-10);
    }


    @Test(expected = IllegalArgumentException.class)
    public void setElement1() {
        DiagonalMatrix m = new DiagonalMatrix(2, 2.0);
        m.setElement(1,2,2);
    }

    @Test
    public void Determinant() {
        DiagonalMatrix m = new DiagonalMatrix(3, -2.0, 2.5, 2.0);
        double a = m.determinant();
        assertEquals(-10, a, 1e-10);
    }

    @Test
    public void Determinant1() {
        DiagonalMatrix m = new DiagonalMatrix(3, 0.0, 0.0, 0.0);
        double a = m.determinant();
        assertEquals(0, a, 1e-10);
    }
}