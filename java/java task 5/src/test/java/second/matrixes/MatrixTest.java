package second.matrixes;

import org.junit.Test;
import second.matrixes.Matrix;

import static org.junit.Assert.assertEquals;


public class MatrixTest {

    @Test
    public void setAndGetElement () {
        Matrix m = new Matrix(2);
        m.setElement(1, 1, 1);
        m.setElement(1, 2, 2);
        m.setElement(2, 1, 3);
        m.setElement(2, 2, 4);
        assertEquals(1,m.getElement(1,1),1e-10);
        assertEquals(2,m.getElement(1,2),1e-10);
        assertEquals(3,m.getElement(2,1),1e-10);
        assertEquals(4,m.getElement(2,2),1e-10);
    }

    @Test
    public void determinant () {
        Matrix m = new Matrix(2, -1, -2, -3, -4);
        double a = m.determinant();
        assertEquals(-2, a ,1e-10);
    }

    @Test
    public void determinant2() {
        Matrix m = new Matrix(3, -1, -1, -2, 1, 2, 1, 2, 2, 2);
        double a = m.determinant();
        assertEquals(2, a,1e-10);
    }

    @Test
    public void determinant3() {
        Matrix m = new Matrix(3, 1, 1, 2, 1, 2, 1, 2, 2, 2);
        double a = m.determinant();
        assertEquals(-2, a,1e-10);
    }

    @Test
    public void determinant4() {
        Matrix m = new Matrix(3, 0,0,0,0,0,0,0,0,0);
        double a = m.determinant();
        assertEquals(0, a,1e-10);
    }

    @Test
    public void determinant5() {
        Matrix m = new Matrix(4, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1);
        double a = m.determinant();
        assertEquals(0, a,1e-10);
    }

    @Test
    public void determinant6() {
        Matrix m = new Matrix(3, 1,0,0,0,1,0,0,0,11);
        double a = m.determinant();
        assertEquals(11, a,1e-10);
    }

    @Test
    public void determinant7() {
        Matrix m = new Matrix(3, 0,0,1,0,1,0,11,0,0);
        double a = m.determinant();
        assertEquals(-11, a,1e-10);
    }
}