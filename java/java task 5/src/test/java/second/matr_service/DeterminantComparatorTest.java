package second.matr_service;

import org.junit.Test;
import second.matrixes.DiagonalMatrix;
import second.matrixes.Matrix;
import second.matrixes.UpTriangleMatrix;

import static org.junit.Assert.assertEquals;


public class DeterminantComparatorTest {

    @Test
    public void compare() {
        Matrix m = new Matrix(2, 1, 2, 3, 4); //-2
        Matrix m1 = new Matrix(3, 1, 2, 3, 4, 4, 3, -2, -1, -1); //7
        DeterminantComparator dC = new DeterminantComparator();
        assertEquals(-1,dC.compare(m,m1));
    }

    @Test
    public void compare1() {
        Matrix m = new Matrix(2, 1, 2, 3, 4); //-2
        Matrix m1 = new Matrix(3, 1, 2, 3, 4, 4, 3, 2, 1, 1); //-7
        DeterminantComparator dC = new DeterminantComparator();
        assertEquals(1,dC.compare(m,m1));
    }

    @Test
    public void compare2() {
        Matrix m = new Matrix(2, 1, 1, 1, 5);
        Matrix m1 = new Matrix(3, 1, 1, 1, 1, -1, 1, 1, -1, -1); //4
        DeterminantComparator dC = new DeterminantComparator();
        int a = dC.compare(m,m1);
        assertEquals(0,dC.compare(m,m1));
    }

    @Test
    public void compare3() {
        DiagonalMatrix m = new DiagonalMatrix(3, 1, 1, 3);
        Matrix m1 = new Matrix(2, 1, 1, 1, 5); //4
        DeterminantComparator dC = new DeterminantComparator();
        int a = dC.compare(m,m1);
        assertEquals(-1,dC.compare(m,m1));
    }

    @Test
    public void compare4() {
        UpTriangleMatrix m = new UpTriangleMatrix(2, new double[] {1, 2, 3});
        Matrix m1 = new Matrix(2, 1, 1, 1, 5); //4
        DeterminantComparator dC = new DeterminantComparator();
        int a = dC.compare(m,m1);
        assertEquals(-1,dC.compare(m,m1));
    }
}