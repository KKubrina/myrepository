package second.matr_service;

import org.junit.Test;
import second.matrixes.DiagonalMatrix;
import second.matrixes.Matrix;
import second.matrixes.UpTriangleMatrix;

import static org.junit.jupiter.api.Assertions.*;

public class MatrixServiceTest {


    @Test
    public void arrangeMatrices() {
        Matrix m = new Matrix(3, 1, 2, 3, 4, 4, 3, 2, 1, 1); //-7
        DiagonalMatrix m1 = new DiagonalMatrix(3, 1, 2, -1); //-2
        UpTriangleMatrix m2 = new UpTriangleMatrix(2, new double[] {1, 5, 2}); //2
        Matrix m3 = new Matrix(2, 1, -2, 3, 4); //11
        Matrix[] arrayMatrix = {m3, m2, m1, m};
        Matrix[] arrayMatrix1 = {m, m1, m2, m3};
        assertArrayEquals(arrayMatrix1, MatrixService.arrangeMatrices(arrayMatrix));
    }

    @Test
    public void arrangeMatrices1() {
        Matrix m = new Matrix(3, 1, 2, 3, 4, 4, 3, 2, 1, 1); //-7
        DiagonalMatrix m1 = new DiagonalMatrix(3, 1, 2, -1); //-2
        UpTriangleMatrix m2 = new UpTriangleMatrix(2, new double[] {1, 5, 2}); //2
        Matrix m3 = new Matrix(2, 1, -2, 3, 4); //11
        Matrix[] arrayMatrix = {m, m1, m2, m3};
        Matrix[] arrayMatrix1 = {m, m1, m2, m3};
        assertArrayEquals(arrayMatrix1, MatrixService.arrangeMatrices(arrayMatrix));
    }

    @Test
    public void arrangeMatrices2() {
        Matrix m = new Matrix(3, 1, 2, 3, 4, 4, 3, 2, 1, 1); //-7
        DiagonalMatrix m1 = new DiagonalMatrix(3, 1, 2, -1); //-2
        UpTriangleMatrix m2 = new UpTriangleMatrix(2, new double[] {1, 5, 2}); //2
        Matrix m3 = new Matrix(2, 1, -2, 3, 4); //11
        Matrix[] arrayMatrix = {m2, m, m3, m1};
        Matrix[] arrayMatrix1 = {m, m1, m2, m3};
        assertArrayEquals(arrayMatrix1, MatrixService.arrangeMatrices(arrayMatrix));
    }
}