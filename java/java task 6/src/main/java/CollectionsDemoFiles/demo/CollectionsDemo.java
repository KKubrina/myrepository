package CollectionsDemoFiles.demo;

import CollectionsDemoFiles.humansClasses.Human;

import java.util.*;

public class CollectionsDemo {
    static public int method1(ArrayList<String> listOfString, char c) {
        int answer = 0;
        for (String s : listOfString) {
            if (s != null && s.charAt(0) == c) {
                answer += 1;
            }
        }
        return answer;
    }

    static public ArrayList<Human> method3(ArrayList<Human> listOfHuman, Human z) {
        ArrayList<Human> answer = new ArrayList<>();
        for (Human human : listOfHuman) {
            if (!z.equals(human)) {
                answer.add(human);
            }

        }
        return answer;
    }

    static public ArrayList<HashSet<Integer>> method4(ArrayList<HashSet<Integer>> ListOfSetOfInt, HashSet<Integer> z) {
//Collections.disjoint();
        ArrayList<HashSet<Integer>> answer = new ArrayList<>();
        int a = 0;
        int b;
        for (HashSet<Integer> integers : ListOfSetOfInt) {
            for (Integer integer : integers) {
                b = integer;
                if (z.add(b)) {
                    z.remove(b);
                } else {
                    a = 1;
                    break;
                }
            }
            if (a != 1) {
                answer.add(integers);
            }
            a = 0;
        }
        return answer;
    }

    static public ArrayList<Human> method7(Map<Integer, Human> z, HashSet<Integer> integ) {
        ArrayList<Human> answer = new ArrayList<>();
        Iterator<Integer> iterator = integ.iterator();
        int b;
        while (iterator.hasNext()) {
            b = iterator.next();
            if (z.get(b) != null) {
                answer.add(z.get(b));
            }
        }

        return answer;
    }

    static public ArrayList<Integer> method8(Map<Integer, Human> z) {
        ArrayList<Integer> answer = new ArrayList<>();


        Set<Integer> intSet = z.keySet();
        for (Integer integer : intSet) {
            if (z.get(integer).getAge() >= 18) {
                answer.add(integer);
            }
        }

        return answer;
    }

    static public Map<Integer, Integer> method9(Map<Integer, Human> z) {
        Map<Integer, Integer> answer = new HashMap<>();

        Set<Integer> intSet = z.keySet();
        for (Integer integer : intSet) {

            answer.put(integer, z.get(integer).getAge());
        }

        return answer;
    }

    static public Map<Integer, ArrayList<Human>> method10(HashSet<Human> humans) {
        Map<Integer, ArrayList<Human>> answer = new HashMap<>();
        HashSet<Integer> ageCode = new HashSet<>();
        ArrayList<Human> ageHuman;
        int age;
        Human human1;
        Human human2;
        for (Human human : humans) {
//            human1 = human;
            age = human.getAge();
            //1. answer.containsKey(age)
            // yes ->
            // no ->
            if (!answer.containsKey(age)) {
                ageHuman = new ArrayList<>();
                answer.put(age, ageHuman);
            }
//            ageHuman =
            answer.get(age).add(human);
            Collections.sort(answer.get(age));
//            answer.put(age, ageHuman);
        }

        return answer;
    }
}
