package CollectionsDemoFiles.demo;

import CollectionsDemoFiles.humansClasses.Human;

import java.util.ArrayList;
import java.util.HashSet;

public class ListDemo {


    static public ArrayList<Human> method2(ArrayList<Human> listOfHuman, Human z) {
        ArrayList<Human> answer = new ArrayList<Human>();
        char[] m;
        int tf = 0;
        int size;
        for (Human human : listOfHuman) {
            m = human.getSurname().toCharArray();
            if (z.getSurname().toCharArray().length == m.length) {

                size = m.length;
            } else if ((z.getSurname().toCharArray().length - m.length) == 1 && z.getSurname().toCharArray()[m.length] == 'а') {
                size = m.length;
            } else if ((m.length - z.getSurname().toCharArray().length) == 1 && m[z.getSurname().toCharArray().length] == 'а') {
                size = z.getSurname().toCharArray().length;
            } else {
                size = -1;
            }
            for (int j = 0; j < size; j++) {
                if (m[j] == z.getSurname().toCharArray()[j]) {
                    tf += 1;
                }
            }
            if (tf == size) {
                answer.add(human);
            }

            tf = 0;
        }
        return answer;
    }


    static public HashSet<Human> method5(ArrayList<Human> listOfHuman) {
        int maxAge = -1;
        for (Human value : listOfHuman) {
            if (value.getAge() > maxAge) {
                maxAge = value.getAge();
            }
        }
        HashSet<Human> answer = new HashSet<>();
        for (Human human : listOfHuman) {
            if (human.getAge() == maxAge) {
                answer.add(human);
            }
        }
        return answer;
    }
}


