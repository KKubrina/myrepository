package CollectionsDemoFiles.humansClasses;

public class Student extends Human {
    String facultyName;

    public Student(String name, String surname, String patronymic, int age, String fName) {
        super(name, surname, patronymic, age);
        this.facultyName = fName;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }
}
