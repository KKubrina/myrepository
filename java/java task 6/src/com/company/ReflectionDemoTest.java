package com.company;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class ReflectionDemoTest {

    @Test
    void method1() {
        Human human1 = new Human("Иван", "Иванов", "Иванович", 40 );
        Student student1 = new Student("Степан", "Сидоров", "Петрович", 55, "Философский");

        ArrayList list = new ArrayList();
        list.add("Привет");
        list.add(human1);
        list.add(student1);
        list.add(122);
        list.add(new String[] {"123", "11", "23"});

        int a  = ReflectionDemo.method1(list);
        assertEquals(2, a);

    }

    @Test
    void method2() {
        Human human1 = new Human("Иван", "Иванов", "Иванович", 40 );

        ArrayList<String> list = new ArrayList<String>();
        list.add("hashCode");
        list.add("equals");
        list.add("setAge");
        list.add("setPatronymic");
        list.add("setSurname");
        list.add("setName");
        list.add("getAge");
        list.add("getName");
        list.add("getPatronymic");
        list.add("getSurname");


        ArrayList<String> list1 = new ArrayList<String>();
        list1 = ReflectionDemo.method2(human1);
        Collections.sort(list);
        Collections.sort(list1);
        assertEquals(list.size(), list1.size());
        for(int i = 0; i<list.size(); i++){
            assertEquals(list.get(i), list1.get(i));

        }

    }

    @Test
    void method3() {
        Student student1 = new Student("Степан", "Сидоров", "Петрович", 55, "Философский");

        ArrayList<String> list = new ArrayList<String>();
        list.add("Human");
        list.add("Object");


        ArrayList<String> list1 = new ArrayList<String>();
        list1 = ReflectionDemo.method3(student1);
        Collections.sort(list);
        Collections.sort(list1);
        assertEquals(list.size(), list1.size());
        for(int i = 0; i<list.size(); i++){
            assertEquals(list.get(i), list1.get(i));

        }

    }


}