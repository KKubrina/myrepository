package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CollectionsDemoTest {

    @org.junit.jupiter.api.Test
    void method1() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("абв");
        arrayList.add("а бв");
        arrayList.add(" абв");
        arrayList.add("фбвa");
        char a = 'а';
        int b = CollectionsDemo.method1(arrayList, a);
        assertEquals(2, b);

    }

    @org.junit.jupiter.api.Test
    void method3() {
        Human human1 = new Human("Иван", "Иванов", "Иванович", 40 );
        Human human2 = new Human("Петр", "Иванов", "Иванович", 42 );
        Human human3 = new Human("Иван", "Петров", "Иванович", 55 );
        Human human4 = new Human("Людмила", "Иванова", "Ивановна", 35 );
        ArrayList arrayList = new ArrayList();
        arrayList.add(human1);
        arrayList.add(human2);
        arrayList.add(human3);
        arrayList.add(human4);
        ArrayList arrayList1 = new ArrayList();
        arrayList1 = CollectionsDemo.method3(arrayList, human3);
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(human1);
        arrayList2.add(human2);
        arrayList2.add(human4);
        assertEquals(arrayList2.size(), arrayList1.size());
        for(int i = 0; i<arrayList2.size(); i++){
            assertEquals(arrayList2.get(i), arrayList1.get(i));
        }

    }

    @org.junit.jupiter.api.Test
    void method4() {
        HashSet<Integer> intSet1 = new HashSet<>();
        intSet1.add(10);
        intSet1.add(15);
        intSet1.add(18);
        HashSet<Integer> intSet2 = new HashSet<>();
        intSet2.add(5);
        intSet2.add(1);
        intSet2.add(8);
        HashSet<Integer> intSet3 = new HashSet<>();
        intSet3.add(3);
        intSet3.add(2);
        intSet3.add(118);

        ArrayList arrayList = new ArrayList();
        arrayList.add(intSet1);
        arrayList.add(intSet2);
        arrayList.add(intSet3);


        ArrayList arrayList1 = new ArrayList();
        arrayList1 = CollectionsDemo.method4(arrayList, intSet3);


        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(intSet1);
        arrayList2.add(intSet2);


        assertEquals(arrayList2.size(), arrayList1.size());
        for(int i = 0; i<arrayList2.size(); i++){
            assertEquals(arrayList2.get(i), arrayList1.get(i));
        }

    }

    @org.junit.jupiter.api.Test
    void method7() {
        Human human1 = new Human("Иван", "Иванов", "Иванович", 40 );
        Human human2 = new Human("Петр", "Иванов", "Петрович", 42 );
        Human human3 = new Human("Кирилл", "Петров", "Кириллович", 55 );
        Human human4 = new Human("Людмила", "Иванова", "Ивановна", 35 );
        Student student1 = new Student("Степан", "Сидоров", "Петрович", 55, "Философский");
        Student student2 = new Student("Анастасия", "Смирнова", "Степановна", 19, "Медицинский");

        Map<Integer, Human> map1 = new HashMap<Integer, Human>();
        map1.put(1, human1);
        map1.put(2, human2);
        map1.put(3, human3);
        map1.put(4, human4);
        map1.put(5, student1);
        map1.put(6, student2);

        HashSet<Integer> intSet1 = new HashSet<>();
        intSet1.add(3);
        intSet1.add(5);
        intSet1.add(6);

        ArrayList arrayList = new ArrayList();
        arrayList.add(human3);
        arrayList.add(student1);
        arrayList.add(student2);

        ArrayList arrayList1 = new ArrayList();
        arrayList1 = CollectionsDemo.method7(map1, intSet1);

        assertEquals(arrayList1.size(), arrayList.size());
        for(int i = 0; i<arrayList1.size(); i++){
            assertEquals(arrayList1.get(i), arrayList.get(i));
        }
    }

    @org.junit.jupiter.api.Test
    void method8() {
        Human human1 = new Human("Иван", "Иванов", "Иванович", 12 );
        Human human2 = new Human("Петр", "Иванов", "Петрович", 13 );
        Human human3 = new Human("Кирилл", "Петров", "Кириллович", 55 );
        Human human4 = new Human("Людмила", "Иванова", "Ивановна", 14 );
        Student student1 = new Student("Степан", "Сидоров", "Петрович", 55, "Философский");
        Student student2 = new Student("Анастасия", "Смирнова", "Степановна", 19, "Медицинский");

        Map<Integer, Human> map1 = new HashMap<Integer, Human>();
        map1.put(1, human1);
        map1.put(2, human2);
        map1.put(3, human3);
        map1.put(4, human4);
        map1.put(5, student1);
        map1.put(6, student2);

        ArrayList<Integer> intList1 = new ArrayList<Integer>();
        intList1.add(3);
        intList1.add(5);
        intList1.add(6);

        ArrayList<Integer> intList2 = new ArrayList<Integer>();
        intList2 = CollectionsDemo.method8(map1);

        assertEquals(intList1.size(), intList2.size());
        for(int i = 0; i<intList1.size(); i++){
            assertEquals(intList1.get(i), intList2.get(i));
        }
    }

    @org.junit.jupiter.api.Test
    void method9() {
        Human human1 = new Human("Иван", "Иванов", "Иванович", 34 );
        Human human2 = new Human("Петр", "Иванов", "Петрович", 44 );
        Human human3 = new Human("Кирилл", "Петров", "Кириллович", 53 );
        Human human4 = new Human("Людмила", "Иванова", "Ивановна", 24 );
        Student student1 = new Student("Степан", "Сидоров", "Петрович", 55, "Философский");
        Student student2 = new Student("Анастасия", "Смирнова", "Степановна", 19, "Медицинский");

        Map<Integer, Human> map1 = new HashMap<Integer, Human>();
        map1.put(1, human1);
        map1.put(2, human2);
        map1.put(3, human3);
        map1.put(4, human4);
        map1.put(5, student1);
        map1.put(6, student2);

        Map<Integer, Human> map2 = new HashMap<Integer, Human>();
        map2.put(34, human1);
        map2.put(44, human2);
        map2.put(53, human3);
        map2.put(24, human4);
        map2.put(55, student1);
        map2.put(19, student2);

        Map<Integer, Human> map3 = new HashMap<Integer, Human>();
        map3 = CollectionsDemo.method9(map1);

        assertEquals(map2, map3);
    }

    @org.junit.jupiter.api.Test
    void method10() {
        Human human1 = new Human("Иван", "Иванов", "Иванович", 34 );
        Human human2 = new Human("Петр", "Иванов", "Петрович", 34 );
        Human human3 = new Human("Кирилл", "Петров", "Кириллович", 55 );
        Human human4 = new Human("Людмила", "Иванова", "Ивановна", 24 );
        Student student1 = new Student("Степан", "Сидоров", "Петрович", 55, "Философский");
        Student student2 = new Student("Анастасия", "Смирнова", "Степановна", 19, "Медицинский");

        HashSet<Human> humans = new HashSet<Human>();
        humans.add(human1);
        humans.add(human2);
        humans.add(human3);
        humans.add(human4);
        humans.add(student1);
        humans.add(student2);

        ArrayList<Human> ageHuman = new ArrayList<Human>();
        ageHuman.add(human1);
        ageHuman.add(human2);
        ArrayList<Human> ageHuman1 = new ArrayList<Human>();
        ageHuman1.add(human3);
        ageHuman1.add(student1);
        ArrayList<Human> ageHuman2 = new ArrayList<Human>();
        ageHuman2.add(human4);
        ArrayList<Human> ageHuman3 = new ArrayList<Human>();
        ageHuman3.add(student2);


        Map<Integer,  ArrayList<Human>> map1 = new HashMap<Integer,  ArrayList<Human>>();
        map1.put(34, ageHuman);
        map1.put(55, ageHuman1);
        map1.put(24, ageHuman2);
        map1.put(19, ageHuman3);


        Map<Integer, ArrayList<Human>> map2 = new HashMap<Integer, ArrayList<Human>>();
        map2 = CollectionsDemo.method10(humans);

        assertEquals(map1, map2);
    }


}