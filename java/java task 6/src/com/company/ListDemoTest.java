package com.company;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class ListDemoTest {

    @org.junit.jupiter.api.Test
    void method2() {
        Human human1 = new Human("Иван", "Иванов", "Иванович", 40 );
        Human human2 = new Human("Петр", "Иванов", "Иванович", 42 );
        Human human3 = new Human("Иван", "Петров", "Иванович", 55 );
        Human human4 = new Human("Людмила", "Иванова", "Ивановна", 35 );
        ArrayList arrayList = new ArrayList();
        arrayList.add(human1);
        arrayList.add(human2);
        arrayList.add(human3);
        arrayList.add(human4);
        ArrayList arrayList1 = new ArrayList();
        arrayList1 = ListDemo.method2(arrayList, human1);
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(human1);
        arrayList2.add(human2);
        arrayList2.add(human4);
        assertEquals(arrayList2.size(), arrayList1.size());
        for(int i = 0; i<arrayList2.size(); i++){
            assertEquals(arrayList2.get(i), arrayList1.get(i));

        }

    }




    @org.junit.jupiter.api.Test
    void method5() {
        Human human1 = new Human("Иван", "Иванов", "Иванович", 40 );
        Human human2 = new Human("Петр", "Иванов", "Петрович", 42 );
        Human human3 = new Human("Кирилл", "Петров", "Кириллович", 55 );
        Human human4 = new Human("Людмила", "Иванова", "Ивановна", 35 );
        Student student1 = new Student("Степан", "Сидоров", "Петрович", 55, "Философский");
        Student student2 = new Student("Анастасия", "Смирнова", "Степановна", 19, "Медицинский");
        ArrayList arrayList = new ArrayList();
        arrayList.add(human1);
        arrayList.add(human2);
        arrayList.add(human3);
        arrayList.add(human4);
        arrayList.add(student1);
        arrayList.add(student2);
        HashSet<Human> humans1  = new HashSet<>();
        humans1  = ListDemo.method5(arrayList);
        HashSet<Human> humans2  = new HashSet<>();
        humans2.add(human3);
        humans2.add(student1);
        assertEquals(humans1, humans2);

    }
}