package com.company;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Objects;

import static java.lang.reflect.Modifier.isPublic;

public class ReflectionDemo {


    static public int method1 (ArrayList list){
        int answer = 0;
        for (int i=0; i<list.size();i++){
            if(list.get(i) instanceof Human){
                answer +=1;
            }
        }
        return answer;
    }

    static public ArrayList<String> method2 (Object object){
        ArrayList<String> strList = new ArrayList<String>();
        Class<?> classData = object.getClass();
        Method[] methods = classData.getDeclaredMethods();
        for(int i = 0; i< methods.length; i++){
            if(isPublic(methods[i].getModifiers())){
            strList.add(methods[i].getName());
            }
        }
        return strList;
    }

    static public ArrayList<String> method3 (Object object){
        ArrayList<String> strList = new ArrayList<String>();
        Class<?> classData = object.getClass();
        Class<?> superclass;
        while(classData.getSuperclass()!=null){
            superclass = classData.getSuperclass();
            strList.add(superclass.getSimpleName());
            classData = superclass;
        }
        return strList;
    }
}
