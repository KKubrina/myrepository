package com.company;

import java.util.*;

public class CollectionsDemo {
    static public int method1 (ArrayList<String> listOfString, char c){
        int answer = 0;
        char[] m = new char[] {};
        for (int i=0; i<listOfString.size();i++){
            m=listOfString.get(i).toCharArray();
           if(m[0]==c){
               answer +=1;
           }
        }
        return answer;
    }

    static public ArrayList<Human> method3(ArrayList<Human> listOfHuman, Human z){
        ArrayList<Human> answer = new ArrayList<Human>();

        for (int i=0; i<listOfHuman.size();i++){
            if (!z.equals(listOfHuman.get(i))) {
                answer.add(listOfHuman.get(i));                }

        }
        return answer;
    }

    static public ArrayList<HashSet<Integer>> method4(ArrayList<HashSet<Integer>> ListOfSetOfInt, HashSet<Integer> z){

        ArrayList<HashSet<Integer>> answer = new ArrayList<HashSet<Integer>>();
        int a=0;
        int b;
        for (int i=0; i<ListOfSetOfInt.size();i++){
            Iterator<Integer> iterator = ListOfSetOfInt.get(i).iterator();
            while (iterator.hasNext()) {
                b = iterator.next();
                if(z.add(b)){
                    z.remove(b);
                }
                else{
                    a=1;
                    break;
                }
            }
            if(a!=1){
                answer.add(ListOfSetOfInt.get(i));
            }
            a = 0;
        }


        return answer;
    }

    static public ArrayList<Human> method7(Map<Integer, Human> z, HashSet<Integer> integ) {
        ArrayList<Human> answer = new ArrayList<Human>();
        Iterator<Integer> iterator = integ.iterator();
        int b;

        while (iterator.hasNext()) {
            b = iterator.next();
            if(z.get(b)!=null){
                answer.add(z.get(b));
            }
        }

        return answer;
    }

    static public ArrayList<Integer> method8(Map<Integer, Human> z) {
        ArrayList<Integer> answer = new ArrayList<Integer>();
        int b;

        Set<Integer> intSet = z.keySet();
        Iterator<Integer> iterator = intSet.iterator();
        while (iterator.hasNext()) {
            b = iterator.next();
            if(z.get(b).getAge() >= 18){
                answer.add(b);
            }
        }

        return answer;
    }

    static public Map<Integer, Human> method9(Map<Integer, Human> z) {
        Map<Integer, Human> answer = new HashMap<Integer, Human>();
        int b;

        Set<Integer> intSet = z.keySet();
        Iterator<Integer> iterator = intSet.iterator();
        while (iterator.hasNext()) {
            b = iterator.next();
            answer.put(z.get(b).getAge(), z.get(b));
        }

        return answer;
    }

    static public Map<Integer, ArrayList<Human>> method10(HashSet<Human> z) {
        Map<Integer, ArrayList<Human>> answer = new HashMap<Integer, ArrayList<Human>>();
        HashSet <Integer> ageCode = new HashSet<Integer>();
        int b;
        int age;
        Human human1;
        Human human2;
        Iterator<Human> iterator = z.iterator();
        while (iterator.hasNext()) {
            human1 = iterator.next();
            age = human1.getAge();
            if( ageCode.add(age)){
                ArrayList<Human> ageHuman = new ArrayList<Human>();
                Iterator<Human> iterator1 = z.iterator();
               while (iterator1.hasNext()) {
                   human2 = iterator1.next();
                   if(human2.getAge() == age){
                       ageHuman.add(human2);
                   }
               }
                answer.put(age, ageHuman);
            }
        }

        return answer;
    }

}
