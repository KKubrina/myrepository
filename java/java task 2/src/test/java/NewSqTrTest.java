
import org.junit.Assert;
import static org.junit.jupiter.api.Assertions.*;

class NewSqTrTest {

    @org.junit.jupiter.api.Test
    void method() {
        NewSqTr nst = new NewSqTr(6, -4, -2); //roots: 1.0, -1.0 / 3
        double a = nst.method();
        assertEquals(1.0, a);
    }

    @org.junit.jupiter.api.Test
    void method1() {
        NewSqTr nst = new NewSqTr(6, 4, -2); //roots: -1.0, 1.0 / 3
        double a = nst.method();
        assertEquals(1.0 / 3, a);
    }


    @org.junit.jupiter.api.Test
    void method2() throws IllegalArgumentException {
        NewSqTr nst = new NewSqTr(5, -4, 1); //roots: net
        try {
            double a = nst.method();
            Assert.fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException thrown) {
            Assert.assertNotEquals("", thrown.getMessage());
        }

    }

    @org.junit.jupiter.api.Test
    void method3() {
        NewSqTr nst = new NewSqTr(4, -4, 1); //root: 1.0 / 2
        double a = nst.method();
        assertEquals(1.0 / 2, a);
    }



}