import equations.SquareTrinomial;

import java.util.Objects;

public class NewSqTr {
    SquareTrinomial squareTrinomial;

    public NewSqTr(int b, int c, int d) {
        this.squareTrinomial = new SquareTrinomial(b, c, d);
    }

    public SquareTrinomial getSquareTrinomial() {
        return squareTrinomial;
    }

    public void setSquareTrinomial(SquareTrinomial squareTrinomial) {
        this.squareTrinomial = squareTrinomial;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NewSqTr)) return false;
        NewSqTr newSqTr = (NewSqTr) o;
        return Objects.equals(getSquareTrinomial(), newSqTr.getSquareTrinomial());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSquareTrinomial());
    }

    public double method() {
        double[] roots;
        roots = squareTrinomial.roots();
        if (roots.length > 1) {
            if (roots[0] > roots[1]) {
                return roots[0];
            }
            return roots[1];
        } else if (roots.length == 1) {
            return roots[0];
        }
        throw new IllegalArgumentException("Нет корней!");


    }

}
