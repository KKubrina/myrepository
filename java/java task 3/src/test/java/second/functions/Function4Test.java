package second.functions;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Function4Test {
    private static final double DELTA = 1e-13;
    @Test
    public void valueCalculation() {
        double answer;
        Function4 f = new Function4(6, -6, 5, 8);
        answer = f.valueCalculation(5);
        assertEquals(750.06579551288301710557790020276, answer, DELTA);
    }

    @Test
    public void valueCalculation1() {
        double answer;
        Function4 f = new Function4(3, -3, 10, 10);
        answer = f.valueCalculation(2);
        assertEquals(83.89056098930650227230427460575, answer, DELTA);
    }

    @Test
    public void valueCalculation2() {
        double answer;
        Function4 f = new Function4(1, -1, 5, 8);
        answer = f.valueCalculation(0);
        assertEquals(13, answer, DELTA);
    }

}