package second.functions;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public  class Function1Test {
    private static final double DELTA = 1e-13;


    @Test
    public void test1() {
        double answer;
        Function1 f = new Function1(2, -2, 2, 3);
        answer = f.valueCalculation(1.5);
        assertEquals(6, answer, DELTA);
    }

    @Test
    public void test2() {
        double answer;
        Function1 f = new Function1(2, -2, 6, 4);
        answer = f.valueCalculation(1.5);
        assertEquals(13, answer, DELTA);
    }

    @Test
    public void test3() {
        double answer;
        Function1 f = new Function1(2, -2, 2, 0);
        answer = f.valueCalculation(1.5);
        assertEquals(3, answer, DELTA);
    }



}