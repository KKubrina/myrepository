package second.functions;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Function3Test {
    private static final double DELTA = 1e-13;

    @Test
    public void valueCalculation() {
        double answer;
        Function3 f = new Function3(3, -3, 4, 6, 2,3);
        answer = f.valueCalculation(2);
        assertEquals(2, answer, DELTA);
    }

    @Test
    public void valueCalculation1() {
        double answer;
        Function3 f = new Function3(9, -9, 2.5, 5, 5 ,10);
        answer = f.valueCalculation(8);
        assertEquals(0.5, answer, DELTA);
    }

    @Test
    public void valueCalculation2() {
        double answer;
        Function3 f = new Function3(11, -11, 1.5, 0, 15,0);
        answer = f.valueCalculation(10);
        assertEquals(0.1, answer, DELTA);
    }

}