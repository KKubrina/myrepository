package second.functions;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class Function2Test {
    private static final double DELTA = 1e-13;

    @Test
    public void valueCalculation() {
        double answer;
        Function2 f = new Function2(2, -2, 8, 1);
        answer = f.valueCalculation(Math.PI/2);
        assertEquals(8, answer, DELTA);
    }

    @Test
    public void valueCalculation1() {
        double answer;
        Function2 f = new Function2(1, -1, 8, 1);
        answer = f.valueCalculation(Math.PI/6);
        assertEquals(4, answer, DELTA);
    }

    @Test
    public void valueCalculation2() {
        double answer;
        Function2 f = new Function2(2, -2, 8, 1);
        answer = f.valueCalculation(-Math.PI/2);
        assertEquals(-8, answer, DELTA);
    }


}