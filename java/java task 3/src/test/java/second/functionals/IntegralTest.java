package second.functionals;


import org.junit.Test;
import second.functions.Function1;
import second.functions.Function2;
import second.functions.Function3;
import second.functions.Function4;

import static org.junit.Assert.assertEquals;

public class IntegralTest {
    private static final double DELTA = 1e-13;

    @Test
    public void valueCalculation() {
        Function1 f = new Function1(2, -2, 5, 4);
        Integral i = new Integral(1,-1);
        double answer = i.valueCalculation(f);
        assertEquals(8, answer, DELTA);
    }

    @Test
    public void valueCalculation1() {
        Function1 f = new Function1(2, -2, 11, 8);
        Integral i = new Integral(-1.8,1.8);
        double answer = i.valueCalculation(f);
        assertEquals(28.8, answer, DELTA);
    }

    @Test
    public void valueCalculation2() {
        Function2 f = new Function2(3, -3, 1, 2);
        Integral i = new Integral(-Math.PI/4,Math.PI/4);
        double answer = i.valueCalculation(f);
        assertEquals(0, answer, DELTA);
    }


    @Test
    public void valueCalculation3() {
        Function3 f = new Function3(4, -4, 4, 6, 2,3);
        Integral i = new Integral(1,2);
        double answer = i.valueCalculation(f);
        assertEquals(2, answer, DELTA);
    }

    @Test
    public void valueCalculation4() {
        Function4 f = new Function4(6, -6, 5, 8);
        Integral i = new Integral(1,2);
        double answer = i.valueCalculation(f);
        assertEquals(31.35377404484453492319856, answer, DELTA);
    }

}