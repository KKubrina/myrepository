package second.functionals;

import org.junit.Test;
import second.functions.Function1;
import second.functions.Function2;
import second.functions.Function3;
import second.functions.Function4;

import static org.junit.Assert.assertEquals;

public class Functional1Test {
    private static final double DELTA = 1e-13;

    @Test
    public void valueCalculation() {
        Function1 f = new Function1(2, -2, 5, 4);
        Functional1 i = new Functional1(1,-1);
        double answer = i.valueCalculation(f);
        assertEquals(12, answer, DELTA);
    }

    @Test
    public void valueCalculation1() {
        Function2 f = new Function2(2, -2, 8, 1);
        Functional1 i = new Functional1(Math.PI/2,-Math.PI/2);
        double answer = i.valueCalculation(f);
        assertEquals(0, answer, DELTA);
    }

    @Test
    public void valueCalculation2() {
        Function3 f = new Function3(4, -4, 4, 6, 2,3);
        Functional1 i = new Functional1(3,1);
        double answer = i.valueCalculation(f);
        assertEquals(6, answer, DELTA);
    }

    @Test
    public void valueCalculation3() {
        Function4 f = new Function4(6, -6, 5, 8);
        Functional1 i = new Functional1(1,-1);
        double answer = i.valueCalculation(f);
        assertEquals(44.43080634815243160, answer, DELTA);
    }
}