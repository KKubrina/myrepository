package second.interfaces;

public interface Functional<T extends FunctionOfOneRealArgument> {
    double valueCalculation (T x);
}
