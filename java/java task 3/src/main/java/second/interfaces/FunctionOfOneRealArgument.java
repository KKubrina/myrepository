package second.interfaces;

public interface FunctionOfOneRealArgument {
    double valueCalculation (double x);
    double getUpBorder();
    double getDownBorder();
}
