package second.functions;

import second.interfaces.FunctionOfOneRealArgument;

public class Function3 implements FunctionOfOneRealArgument {
    double A;
    double B;
    double C;
    double D;
    double downBorder;
    double upBorder;

    public Function3(double upB1, double downB1, double A1, double B1, double C1, double D1) {
        A = A1;
        B = B1;
        C = C1;
        D = D1;
        if (((C*upB1 + D) != 0) && ((C*downB1+D) !=0))  {
            if (upB1 < downB1) {
                downBorder = upB1;
                upBorder = downB1;
            }
            else {
                downBorder = downB1;
                upBorder = upB1;
            }}
        else {throw new IllegalArgumentException("Недопустимые параметры");}
}

    public Function3() {
        A = 1.0;
        B = 1.0;
        C = 1.0;
        D = 1.0;
        downBorder = 0;
        upBorder = 1;

    }

    public double getDownBorder() {
        return downBorder;
    }

    public void setDownBorder(double downBorder) {
        this.downBorder = downBorder;
    }

    public double getUpBorder() {
        return upBorder;
    }

    public void setUpBorder(double upBorder) {
        this.upBorder = upBorder;
    }

    public double valueCalculation (double x){
        if (x<upBorder && x>downBorder){
            if ((C*x + D) != 0) {
                return (A*x + B)/(C*x + D); }
            else {throw new IllegalArgumentException("Недопустимые параметры");}
        }
        else {throw new IllegalArgumentException("Значение выходит за границы");}
    }


}

