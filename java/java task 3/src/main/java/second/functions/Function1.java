package second.functions;

import second.interfaces.FunctionOfOneRealArgument;
import java.util.Objects;

public class Function1 implements FunctionOfOneRealArgument {

    double A;
    double B;
    double downBorder;
    double upBorder;

    public double getDownBorder() {
        return downBorder;
    }


    public double getUpBorder() {
        return upBorder;
    }



    public Function1(double upB1, double downB1, double A1, double B1) {
        A = A1;
        B = B1;
        if (upB1 < downB1) {
            downBorder = upB1;
            upBorder = downB1;
        }
        else {
            downBorder = downB1;
            upBorder = upB1;
        }
    }

    public Function1() {
            A = 1.0;
        B = 1.0;
        downBorder = 0;
        upBorder = 1;

    }


    public double getA() {
        return A;
    }

    public void setA(double a1) {
        A = a1;
    }

    public double getB() {
        return B;
    }

    public void setB(double b1) {
        B = b1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Function1)) return false;
        Function1 function1 = (Function1) o;
        return Double.compare(function1.A, A) == 0 &&
                Double.compare(function1.B, B) == 0 &&
                Double.compare(function1.downBorder, downBorder) == 0 &&
                Double.compare(function1.upBorder, upBorder) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(A, B, downBorder, upBorder);
    }

    public double valueCalculation (double x){
        if (x<upBorder && x>downBorder){
           return A*x + B;
        }
        throw new IllegalArgumentException("Значение выходит за границы");
    }

}
