package second.functions;

import second.interfaces.FunctionOfOneRealArgument;

public class Function2 implements FunctionOfOneRealArgument {
    double A;
    double B;
    double downBorder;
    double upBorder;

    public Function2(double upB1, double downB1, double A1, double B1) {
        A = A1;
        B = B1;
        if (upB1 < downB1) {
            downBorder = upB1;
            upBorder = downB1;
        }
        else {
            downBorder = downB1;
            upBorder = upB1;
        }
    }

    public Function2() {
        A = 1.0;
        B = 1.0;
        downBorder = 0;
        upBorder = 1;

    }

    public double getDownBorder() {
        return downBorder;
    }

    public void setDownBorder(double downBorder) {
        this.downBorder = downBorder;
    }

    public double getUpBorder() {
        return upBorder;
    }

    public void setUpBorder(double upBorder) {
        this.upBorder = upBorder;
    }

    public double valueCalculation (double x){
        if (x<upBorder && x>downBorder){
            return A*(Math.sin(x*B));}
        throw new IllegalArgumentException("Значение выходит за границы");
    }

}