package second.functionals;

import second.interfaces.FunctionOfOneRealArgument;
import second.interfaces.Functional;

public class Integral<T extends FunctionOfOneRealArgument> implements Functional<T> {
    double a;
    double b;

    public Integral(double a, double b) {
        if (b < a) {
            this.a = b;
            this.b = a;
        }
        else {
            this.a = a;
            this.b = b;
        }
    }

    public Integral() {
        this.a = 0;
        this.b = 1;
    }

    public double valueCalculation(FunctionOfOneRealArgument x){
        if (x.getDownBorder() < a && x.getUpBorder() > b){
            double step = (b-a)/100;
            double sum= 0 ;
            for (double i = a; i < b; i+=step){
                sum += x.valueCalculation((i+i+step)/2)*step;
            }
            return sum;
        }
         throw new IllegalArgumentException("Границы интегрирования выходят за область определения");
    }
}
