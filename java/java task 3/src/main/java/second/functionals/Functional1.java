package second.functionals;

import second.interfaces.FunctionOfOneRealArgument;
import second.interfaces.Functional;

public class Functional1<T extends FunctionOfOneRealArgument> implements Functional<T> {
    double a;
    double b;

    public Functional1(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public Functional1() {
        this.a = 0;
        this.b = 1;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double valueCalculation(FunctionOfOneRealArgument x){
        return x.valueCalculation(a)+x.valueCalculation(b)+x.valueCalculation((a+b)/2);
    }
}
