package RefDem.test;


import CollectionsDemoFiles.humansClasses.Human;
import CollectionsDemoFiles.humansClasses.Student;
import RefDem.ReflectionDemo;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;

public class ReflectionDemoTest {

    @Test
    public void method1() {
        Human human1 = new Human("Иван", "Иванов", "Иванович", 40);
        Student student1 = new Student("Степан", "Сидоров", "Петрович", 55, "Философский");

        ArrayList<Object> list = new ArrayList<>();
        list.add("Привет");
        list.add(human1);
        list.add(student1);
        list.add(122);
        list.add(new String[]{"123", "11", "23"});

        int a = ReflectionDemo.method1(list);
        assertEquals(2, a);

    }

    @Test
    public void method1a() {
        Human human1 = new Human("Иван", "Иванов", "Иванович", 40);
        Student student1 = new Student("Степан", "Сидоров", "Петрович", 55, "Философский");

        ArrayList<Object> list = new ArrayList<>();
        list.add("Привет");
        list.add(human1);
        list.add(human1);
        list.add(student1);
        list.add(122);
        list.add(new String[]{"123", "11", "23"});

        int a = ReflectionDemo.method1(list);
        assertEquals(3, a);

    }

    @Test
    public void method1b() {

        ArrayList<Object> list = new ArrayList<>();
        list.add("Привет");
        list.add(122);
        list.add(new String[]{"123", "11", "23"});

        int a = ReflectionDemo.method1(list);
        assertEquals(0, a);

    }

    @Test
    public void method2() {
        Human human1 = new Human("Иван", "Иванов", "Иванович", 40);

        ArrayList<String> list = new ArrayList<>();
        list.add("hashCode");
        list.add("equals");
        list.add("setAge");
        list.add("setPatronymic");
        list.add("setSurname");
        list.add("compareTo");
        list.add("getAge");
        list.add("getName");
        list.add("getPatronymic");
        list.add("getSurname");
        list.add("setName");
        list.add("compareTo");


        ArrayList<String> list1;
        list1 = ReflectionDemo.method2(human1);
        Collections.sort(list);
        Collections.sort(list1);
        assertEquals(list.size(), list1.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), list1.get(i));

        }

    }

    @Test
    public void method2a() {
        Student student1 = new Student("Степан", "Сидоров", "Петрович", 55, "Философский");

        ArrayList<String> list = new ArrayList<>();
        list.add("getFacultyName");
        list.add("setFacultyName");


        ArrayList<String> list1;
        list1 = ReflectionDemo.method2(student1);
        Collections.sort(list);
        Collections.sort(list1);
        assertEquals(list.size(), list1.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), list1.get(i));

        }

    }

    @Test
    public void method3() {
        Student student1 = new Student("Степан", "Сидоров", "Петрович", 55, "Философский");

        ArrayList<String> list = new ArrayList<>();
        list.add("Human");
        list.add("Object");


        ArrayList<String> list1;
        list1 = ReflectionDemo.method3(student1);
        Collections.sort(list);
        Collections.sort(list1);
        assertEquals(list.size(), list1.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), list1.get(i));

        }

    }

    @Test
    public void method3a() {
        HashSet<Integer> intSet = new HashSet<>();
        intSet.add(3);
        intSet.add(5);
        intSet.add(6);

        ArrayList<String> list = new ArrayList<>();
        list.add("Object");
        list.add("AbstractSet");
        list.add("AbstractCollection");


        ArrayList<String> list1;
        list1 = ReflectionDemo.method3(intSet);
        Collections.sort(list);
        Collections.sort(list1);
        assertEquals(list.size(), list1.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), list1.get(i));

        }

    }


}