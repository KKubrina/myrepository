package RefDem;

import CollectionsDemoFiles.humansClasses.Human;

import java.lang.reflect.Method;
import java.util.ArrayList;

import static java.lang.reflect.Modifier.isPublic;

public class ReflectionDemo {


    static public int method1 (ArrayList<Object> list){
        int answer = 0;
        for (Object o : list) {
            if (o instanceof Human) {
                answer += 1;
            }
        }
        return answer;
    }

    static public ArrayList<String> method2 (Object object){
        ArrayList<String> strList = new ArrayList<>();
        Class<?> classData = object.getClass();
        Method[] methods = classData.getDeclaredMethods();
        for (Method method : methods) {
            if (isPublic(method.getModifiers())) {
                strList.add(method.getName());
            }
        }
        return strList;
    }

    static public ArrayList<String> method3 (Object object){
        ArrayList<String> strList = new ArrayList<>();
        Class<?> classData = object.getClass();
        Class<?> superclass = classData.getSuperclass();
        while(superclass != null){
            strList.add(superclass.getSimpleName());
            superclass = superclass.getSuperclass();
        }
        return strList;
    }
}
