DROP DATABASE IF EXISTS shop;
CREATE DATABASE `shop`;
USE `shop`;

SET character_set_server = utf8;
SET character_set_client = utf8;
SET character_set_results = utf8;
SET character_set_connection = utf8;

CREATE TABLE user (
  id INT(11) NOT NULL AUTO_INCREMENT,
  fullName VARCHAR(50) NOT NULL,
  login VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  email VARCHAR(50) NOT NULL,
  address VARCHAR(50) NOT NULL,
  phoneNumber VARCHAR(50) NOT NULL,
  UNIQUE KEY login (login),
  UNIQUE KEY phoneNumber (phoneNumber),
  PRIMARY KEY(id)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;


    CREATE TABLE brand (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  country VARCHAR(50) NOT NULL,
  PRIMARY KEY(id)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;

  CREATE TABLE category (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY(id)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;

    CREATE TABLE discountCard (
  cardNumber INT(11) NOT NULL AUTO_INCREMENT,
  validity VARCHAR(50) NOT NULL,
  idUser INT(11) NOT NULL,
  amountDiscount INT(11) NOT NULL,
  FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE,
  PRIMARY KEY(cardNumber)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;


  CREATE TABLE product (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  description VARCHAR(50) NOT NULL,
  price INT NOT NULL,
  idCategory INT(11),
  idBrand INT(11),
  FOREIGN KEY (idCategory) REFERENCES category (id) ON DELETE SET NULL,
  FOREIGN KEY (idBrand) REFERENCES brand (id) ON DELETE SET NULL,
  PRIMARY KEY(id)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;



  CREATE TABLE review (
  idUser INT(11) NOT NULL,
  idProduct INT(11) NOT NULL,
  text VARCHAR(50) NOT NULL,
  data VARCHAR(50) NOT NULL,
  FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE,
  FOREIGN KEY (idProduct) REFERENCES product (id) ON DELETE CASCADE
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;



   CREATE TABLE `order` (
   id INT(11) NOT NULL AUTO_INCREMENT,
   idUser INT(11) NOT NULL,
   amountProducts INT(11) NOT NULL,
   orderDate VARCHAR(50) NOT NULL,
   totalCost INT(11) NOT NULL, 
   FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE,
   PRIMARY KEY(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;


 CREATE TABLE product_order (
   idProduct INT(11) NOT NULL,
   idUser INT(11) NOT NULL,
   amountProduct INT(11) NOT NULL,
   FOREIGN KEY (idProduct) REFERENCES product (id) ON DELETE CASCADE,
   FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8;


CREATE INDEX user_id ON user(id);
CREATE INDEX product_id ON product(id);
CREATE INDEX product_price ON product(price);

delimiter \
CREATE TRIGGER addNewDiscount
AFTER INSERT ON user
FOR EACH ROW
BEGIN
INSERT INTO discountCard(validity, idUser, amountDiscount ) VALUES ('бессрочно', new.id, 0);
END
\


CREATE TRIGGER deleteDiscount
AFTER DELETE ON `order`
FOR EACH ROW
BEGIN
 UPDATE discountCard SET validity = 'бессрочно',
 amountDiscount = 0 
 WHERE idUser = old.idUser;
 END
\


CREATE TRIGGER applyDiscount
BEFORE INSERT ON `order`
FOR EACH ROW
BEGIN
 DECLARE 
 discount int;
 SET discount=(select amountDiscount from discountCard where idUser=new.idUser);
 SET new.totalCost=(new.totalCost/100 * (100 - discount));
 END
\


CREATE TRIGGER addDiscount
AFTER INSERT ON `order`
FOR EACH ROW
BEGIN
IF  new.totalCost > 100000 AND new.totalCost < 1000000 THEN 
UPDATE discountCard SET validity = concat('действует с ', new.orderDate ),
 amountDiscount = 5 
 WHERE idUser = new.idUser;
END IF;
IF  new.totalCost > 1000000 AND new.totalCost < 5000000 THEN 
UPDATE discountCard SET validity = concat('действует с ', new.orderDate ),
 amountDiscount = 15 
 WHERE idUser = new.idUser;END IF;
IF  new.totalCost > 5000000 THEN 
UPDATE discountCard SET validity = concat('действует с ', new.orderDate ),
 amountDiscount = 25 
 WHERE idUser = new.idUser;END IF;
END
\

CREATE TRIGGER addDiscountUpd
AFTER UPDATE ON `order`
FOR EACH ROW
BEGIN
IF  new.totalCost > 100000 AND new.totalCost < 1000000 THEN 
UPDATE discountCard SET validity = concat('действует с ', new.orderDate ),
 amountDiscount = 5 
 WHERE idUser = new.idUser;
END IF;
IF  new.totalCost > 1000000 AND new.totalCost < 5000000 THEN 
UPDATE discountCard SET validity = concat('действует с ', new.orderDate ),
 amountDiscount = 15 
 WHERE idUser = new.idUser;END IF;
IF  new.totalCost > 5000000 THEN 
UPDATE discountCard SET validity = concat('действует с ', new.orderDate ),
 amountDiscount = 25 
 WHERE idUser = new.idUser;
 END IF;
END
\
delimiter ;


 INSERT user VALUES (11, 'Иванов Иван', 'Ivanov_I', '123ivan', 'Ivanov@mail.ru', 'Пушкина, д. 2, кв. 8', '888912');
 INSERT user VALUES (12, 'Иванова Ольга', 'Ivanova_Olga', '11ivanova', 'Ivanova@mail.ru', 'Пушкина, д. 2, кв. 8', '888913');
 INSERT user VALUES (13, 'Смирнова Виктория', 'VikaSm', 'vika001', 'Viktoria@mail.ru', 'Лермантова, д. 12, кв. 31', '313921');
 INSERT user VALUES (14, 'Смирнов Виктор', 'VikTorS', '0viktor001', 'Viktor@mail.ru', 'Омская, д. 141, кв. 19', '273916');
 
 INSERT brand VALUES (1, 'LG', 'Южная Корея');
 INSERT brand VALUES (2, 'Sumsung', 'Южная Корея');
 INSERT brand VALUES (3, 'Apple', 'USA');
 
 INSERT category VALUES (2, 'холодильники');
 INSERT category VALUES (3, 'телефоны');
 
 INSERT product VALUES (20, 'Холодильник LG  HS761', 'Белый двухкамерный холодильник', 3200000, 2, 1);
 INSERT product VALUES (21, 'Холодильник LG  HS31', 'Белый однокамерный холодильник', 1600000, 2, 1);
 INSERT product VALUES (22, 'Холодильник Sumsung  RB36T774FB1', 'Двухкамерный серый холодильник', 4100000, 2, 2);
 INSERT product VALUES (23, 'Телефон Sumsung Galaxy S6', 'Сенсорный черный телефон', 1100000, 3, 2);
 INSERT product VALUES (24, 'Телефон IPhone 7', 'Черный телефон', 2100000, 3, 3);
 
 INSERT `order` VALUES (31, 11, 2, '01.02.2021', 4300000);
 INSERT product_order VALUES (21, 11, 1);
 INSERT product_order VALUES (23, 11, 1);
 INSERT `order` VALUES (32, 12, 2, '02.02.2021', 6200000);
 INSERT product_order VALUES (22, 12, 1);
 INSERT product_order VALUES (24, 12, 1);
 INSERT `order` VALUES (33, 13, 1, '12.01.2021', 2100000);
 INSERT product_order VALUES (23, 13, 1);
 INSERT product_order VALUES (24, 13, 1);
 INSERT `order` VALUES (34, 13, 1, '12.01.2021', 3200000);
 INSERT product_order VALUES (21, 13, 1);


 SELECT * FROM discountCard; 
 SELECT * FROM `order`;
 
 
delimiter \ 
 CREATE PROCEDURE ordersByDate (in dateOr VARCHAR(50))
BEGIN  
SELECT fullName, address, phoneNumber, amountProducts, orderDate, totalCost
FROM user
JOIN `order` ON user.id = `order`.idUser 
where `order`.orderDate = dateOr;
END
\


 CREATE PROCEDURE popularProducts ()
BEGIN  
SELECT product.name, SUM(c1.amountProduct) as amount FROM product_order as c1, product
WHERE EXISTS (SELECT product_order.idProduct FROM product_order 
WHERE c1.amountProduct >= product_order.amountProduct ) 
and product.id = c1.idProduct
GROUP BY idProduct 
ORDER BY SUM(c1.amountProduct) DESC;
END
\

 CREATE PROCEDURE brand_category_productsByPrice ()
BEGIN  
SELECT product.name, description, price, brand.name as brand, category.name as category
FROM product
JOIN brand ON product.idBrand =  brand.id
JOIN category ON product.idCategory =  category.id ORDER BY price;
END
\

CREATE FUNCTION 
correctName(tekst VARCHAR(50)) returns VARCHAR(50)
BEGIN  
DECLARE 
result VARCHAR(50);
DECLARE 
kol INT(10);
 SET result = TRIM(tekst); 
 SET kol = LENGTH(result); 
 IF kol > 0 THEN 
 WHILE(LOCATE('  ', result)<>0) do
 SET result := REPLACE(result,'  ',' ');
 END WHILE;
 SET result := REPLACE(result,'- ','-');
 SET result := REPLACE(result,' -','-'); 
 SET kol := LENGTH(result); 
 ELSE
 SET result := '0'; 
 END IF;
 return result;
END
\

CREATE FUNCTION checkDiscountUser(loginUs VARCHAR(50)) returns INT(11)
BEGIN  
return (select amountDiscount from user, discountCard 
where user.id = discountCard.idUser and user.login = loginUs); 
END
\


CREATE FUNCTION userByNumberCard(cardNum INT(11)) returns VARCHAR(50)
BEGIN  
return (select fullName from user, discountCard 
where user.id = discountCard.idUser and discountCard.cardNumber = cardNum); 
END
\

CREATE FUNCTION mostPopularProductOfCategory(categoryName VARCHAR(50)) returns VARCHAR(50)
BEGIN  
return (SELECT product.name FROM product_order as c1, product, category
WHERE EXISTS (SELECT product_order.idProduct FROM product_order 
WHERE c1.amountProduct >= product_order.amountProduct ) 
and product.id = c1.idProduct 
and product.idCategory = category.id
and category.name = categoryName
GROUP BY idProduct 
ORDER BY SUM(c1.amountProduct) DESC LIMIT 1); 
END
\

CREATE FUNCTION mostPopularProductOfBrand(brandName VARCHAR(50)) returns VARCHAR(50)
BEGIN  
return (SELECT product.name FROM product_order as c1, product, brand
WHERE EXISTS (SELECT product_order.idProduct FROM product_order 
WHERE c1.amountProduct >= product_order.amountProduct ) 
and product.id = c1.idProduct 
and product.idBrand = brand.id
and brand.name = brandName
GROUP BY idProduct 
ORDER BY SUM(c1.amountProduct) DESC LIMIT 1); 
END
\

delimiter ;


call ordersByDate('12.01.2021');
call popularProducts;
call brand_category_productsByPrice;

select correctName(' Арнольд    Дор - Диор  ');

select mostPopularProductOfCategory("холодильники");

select mostPopularProductOfBrand("Sumsung");

select checkDiscountUser("Ivanov_I");

select userByNumberCard(1);

 /*
 #EXEPT
 SELECT * FROM product WHERE idCategory = 2 and
 id NOT IN
 (SELECT id FROM product WHERE idBrand = 2);  
 
 #INTERSECT
 SELECT * FROM product WHERE idCategory = 2 and
 id IN
 (SELECT id FROM product WHERE idBrand = 2);
 
 #UNION
 SELECT * FROM product WHERE idCategory = 2 
 union
 (SELECT * FROM product WHERE idBrand = 2);


SELECT DISTINCT user.fullName
  FROM product_order, user
  WHERE product_order.idUser = user.id and NOT EXISTS
    (SELECT *
        FROM product
        WHERE idCategory=3 and NOT EXISTS
          (SELECT *
              FROM product_order as t1
              WHERE
                  t1.idUser = product_order.idUser and
                  t1.idProduct = product.id));  #кто покупал все телефоны
                  

SELECT product.*, product_order.*, `order`.*, user.*  #эквисоединение
FROM product, product_order, `order`, user 
WHERE product.id = product_order.idProduct and
user.id = product_order.idUser and
`order`.idUser = user.id;

SELECT product.name, product.id, product_order.amountProduct, `order`.orderDate, user.fullName, user.id  #естественное соединение
FROM product, product_order, `order`, user 
WHERE product.id = product_order.idProduct and
user.id = product_order.idUser and
`order`.idUser = user.id;

SELECT product.name, product_order.amountProduct, `order`.orderDate, user.fullName   #композиция
FROM product, product_order, `order`, user 
WHERE product.id = product_order.idProduct and
user.id = product_order.idUser and
`order`.idUser = user.id;


#JOIN
SELECT product.name, product_order.amountProduct, `order`.orderDate, user.fullName 
FROM product
JOIN product_order ON product.id = product_order.idProduct
JOIN user ON user.id = product_order.idUser
JOIN `order` USING (idUser);


SELECT * FROM user INNER JOIN `order` ON `order`.idUser = user.id;

SELECT * FROM user LEFT JOIN `order` ON `order`.idUser = user.id; #внешнее

SELECT * FROM user RIGHT JOIN `order` ON `order`.idUser = user.id;


SELECT * FROM product, `order` WHERE price > totalCost; #тета

SELECT * FROM `order`, `order` copy WHERE `order`.id <> copy.id AND `order`.orderDate=copy.orderDate; 

SELECT DISTINCT `order`.*  FROM `order`, `order` copy WHERE `order`.idUser = copy.idUser; */
