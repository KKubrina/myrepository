DROP DATABASE IF EXISTS task4;
CREATE DATABASE `task4`;
USE `task4`;

CREATE TABLE abid (
  a INT(11),
  b DOUBLE,
  id INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

TYPE a*b*id IS RECORD 
 (a abid.a%TYPE, 
 b abid.b%TYPE,
 id abid.id%TYPE
 )

delimiter \
CREATE TRIGGER addB
BEFORE INSERT ON abid
FOR EACH ROW
BEGIN
SET new.b = (new.a*0.01);
END
\
delimiter ;

INSERT abid(a,b) VALUES (RAND()*(10-1)+1, 0);
INSERT abid(a,b) VALUES (RAND()*(10-1)+1, 0);
INSERT abid(a,b) VALUES (RAND()*(10-1)+1, 0);
INSERT abid(a,b) VALUES (RAND()*(10-1)+1, 0);
INSERT abid(a,b) VALUES (RAND()*(10-1)+1, 0);


TYPE abrec =
 (a abid.a%TYPE, 
 b abid.b%TYPE,
 id abid.id%TYPE
 )

delimiter \
CREATE PROCEDURE proc()
BEGIN
DECLARE
mediana DOUBLE;
DECLARE
middleOt DOUBLE;
DECLARE
middleSqOt DOUBLE;
DECLARE
medianaNew DOUBLE;
DECLARE
middleOtNew DOUBLE;
DECLARE
middleSqOtNew DOUBLE;
DECLARE
aRead INT(11);
DECLARE
bRead DOUBLE;
DECLARE
idRead INT(11);
DECLARE
countRead INT(11);
DECLARE
`count` INT(11);
DECLARE
middleEl INT(11);
DECLARE
ab CURSOR for SELECT * FROM abid ORDER BY a;
DECLARE
abnew CURSOR for SELECT a as anew, a*a as bnew, id FROM abid as abidnew ORDER BY anew;


 IF (select count(a) from abid)%2 <> 0 then
 set middleEl = FLOOR((select count(a) from abid)/2)+1;
 ELSE 
 set middleEl =FLOOR((select count(a) from abid)/2);
 END IF;
 
 set `count` = (select count(a) from abid);
 
 set countRead = 0;
 set middleOt = 0;
 set middleSqOt = 0;
 
 OPEN ab; 
 loop_read:LOOP 
   FETCH ab INTO aRead, bRead, idRead; 
     set countRead = countRead + 1;

     IF countRead = middleEl THEN
       SET mediana = bRead;
     END IF;
 
     IF `count`%2 = 0 AND countRead = middleEl+1 THEN
       SET mediana = mediana + bRead;
       SET mediana = mediana/2;
     END IF;
 
     set middleOt = middleOt + middleOt(bRead, (select sum(b) from abid)/`count`, 1/`count`);
     set middleSqOt = middleOt + middleOt(bRead, (select sum(b) from abid)/`count`, `count`/(`count`-1));

     IF `count` = countRead THEN 
     LEAVE loop_read;
     END IF;
 
 END LOOP; -- Конец цикла
 
 
 
 set countRead = 0;
 set middleOtNew = 0;
 set middleSqOtNew = 0;
 
  OPEN abnew; 
 loop_read:LOOP 
   FETCH abnew INTO aRead, bRead, idRead; 
     set countRead = countRead + 1;

     IF countRead = middleEl THEN
       SET medianaNew = bRead;
     END IF;
 
     IF `count`%2 = 0 AND countRead = middleEl+1 THEN
       SET medianaNew = medianaNew + bRead;
       SET medianaNew = medianaNew/2;
     END IF;
 
     set middleOtNew = middleOtNew + middleOt(bRead, (select sum(a*a) from abid)/`count`, 1/`count`);
     set middleSqOtNew = middleOtNew + middleOt(bRead, (select sum(a*a) from abid)/`count`, `count`/(`count`-1));

     IF `count` = countRead THEN 
     LEAVE loop_read;
     END IF;
 
 END LOOP; -- Конец цикла
 CLOSE abnew; -- Закрытие курсора
 


 select mediana 'Медиана id/a/b', middleOt 'Среднее отклонение id/a/b', middleSqOt 'Среднее квадратичное отклонение id/a/b';

 select medianaNew 'Медиана id/a`/b`', middleOtNew 'Среднее отклонение id/a`/b`', middleSqOtNew 'Среднее квадратичное отклонение id/a`/b`';


 
END; 
\

CREATE FUNCTION middleOt(x DOUBLE, srArif DOUBLE, n DOUBLE) returns DOUBLE
BEGIN  
return sqrt(pow((x - srArif), 2))*n;
END
\


delimiter ;

SELECT * FROM abid ORDER BY a;
SELECT a as anew, a*a as bnew, id FROM abid as abidnew ORDER BY anew;
call proc();

