DROP DATABASE IF EXISTS shop;
CREATE DATABASE `shop`;
USE `shop`;

CREATE TABLE user (
  id INT(11) NOT NULL AUTO_INCREMENT,
  fullName VARCHAR(50) NOT NULL,
  login VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  email VARCHAR(50) NOT NULL,
  address VARCHAR(50) NOT NULL,
  phoneNumber VARCHAR(50) NOT NULL,
  UNIQUE KEY login (login),
  UNIQUE KEY phoneNumber (phoneNumber),
  PRIMARY KEY(id)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;


    CREATE TABLE brand (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  country VARCHAR(50) NOT NULL,
  PRIMARY KEY(id)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;

  CREATE TABLE category (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY(id)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;

    CREATE TABLE discountCard (
  cardNumber INT(11) NOT NULL AUTO_INCREMENT,
  fullName VARCHAR(50) NOT NULL,
  validity VARCHAR(50) NOT NULL,
  idUser INT(11) NOT NULL,
  FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE,
  PRIMARY KEY(cardNumber)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;


  CREATE TABLE product (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  description VARCHAR(50) NOT NULL,
  price INT NOT NULL,
  idCategory INT(11),
  idBrand INT(11),
  FOREIGN KEY (idCategory) REFERENCES category (id) ON DELETE SET NULL,
  FOREIGN KEY (idBrand) REFERENCES brand (id) ON DELETE SET NULL,
  PRIMARY KEY(id)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;



  CREATE TABLE review (
  idUser INT(11) NOT NULL,
  idProduct INT(11) NOT NULL,
  text VARCHAR(50) NOT NULL,
  data VARCHAR(50) NOT NULL,
  FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE,
  FOREIGN KEY (idProduct) REFERENCES product (id) ON DELETE CASCADE
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;



   CREATE TABLE `order` (
   id INT(11) NOT NULL AUTO_INCREMENT,
   idUser INT(11) NOT NULL,
   amountProducts INT(11) NOT NULL,
   orderDate VARCHAR(50) NOT NULL,
   totalCost INT(11) NOT NULL, 
   FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE,
   PRIMARY KEY(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

 CREATE TABLE product_order (
   idProduct INT(11) NOT NULL,
   idUser INT(11) NOT NULL,
   amountProduct INT(11) NOT NULL,
   FOREIGN KEY (idProduct) REFERENCES product (id) ON DELETE CASCADE,
   FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8;



 INSERT user VALUES (11, 'Иванов Иван', 'Ivanov_I', '123ivan', 'Ivanov@mail.ru', 'Пушкина, д. 2, кв. 8', '888912');
 INSERT user VALUES (12, 'Иванова Ольга', 'Ivanova_Olga', '11ivanova', 'Ivanova@mail.ru', 'Пушкина, д. 2, кв. 8', '888913');
 INSERT user VALUES (13, 'Смирнова Виктория', 'VikaSm', 'vika001', 'Viktoria@mail.ru', 'Лермантова, д. 12, кв. 31', '313921');
 INSERT user VALUES (14, 'Смирнов Виктор', 'VikTorS', '0viktor001', 'Viktor@mail.ru', 'Омская, д. 141, кв. 19', '273916');
 
 INSERT brand VALUES (1, 'LG', 'Южная Корея');
 INSERT brand VALUES (2, 'Sumsung', 'Южная Корея');
 INSERT brand VALUES (3, 'Apple', 'USA');
 
 INSERT category VALUES (2, 'холодильники');
 INSERT category VALUES (3, 'телефоны');
 
 INSERT product VALUES (21, 'Холодильник LG  HS761', 'Белый двухкамерный холодильник', 3200000, 2, 1);
 INSERT product VALUES (22, 'Холодильник Sumsung  RB36T774FB1', 'Двухкамерный серый холодильник', 4100000, 2, 2);
 INSERT product VALUES (23, 'Телефон Sumsung Galaxy S6', 'Сенсорный черный телефон', 1100000, 3, 2);
 INSERT product VALUES (24, 'Телефон IPhone 7', 'Черный телефон', 2100000, 3, 3);
 
 INSERT `order` VALUES (31, 11, 2, '01.02.2021', 4300000);
 INSERT product_order VALUES (21, 11, 1);
 INSERT product_order VALUES (23, 11, 1);
 INSERT `order` VALUES (32, 12, 2, '02.02.2021', 6200000);
 INSERT product_order VALUES (22, 12, 1);
 INSERT product_order VALUES (24, 12, 1);
 INSERT `order` VALUES (33, 13, 1, '12.01.2021', 2100000);
 INSERT product_order VALUES (24, 13, 1);
 INSERT `order` VALUES (34, 13, 1, '12.01.2021', 3200000);
 INSERT product_order VALUES (21, 13, 1);
 
 

 SELECT orderDate, totalCost from `order`; 
 SELECT * from product;
 SELECT name from product;
 SELECT ALL user.address, orderDate FROM `order`, user WHERE user.id=idUser; #все адреса для доставки товара
 SELECT DISTINCT user.address, orderDate FROM `order`, user WHERE user.id=idUser; #все уникальные адреса
 SELECT DISTINCT idUser, CASE amountProducts 
 WHEN 1 THEN 'В подарок батарейки!'
 WHEN 2 THEN 'В подарок весы!'
 ELSE 'В подарок наушники!'
 END action FROM `order`; #подарки для покупателей


SELECT * FROM user WHERE login LIKE '%?_%' ESCAPE '?';

 SELECT name as name_of_product from product;
 SELECT concat('заказ сделан - ', orderDate, ', на имя - ', user.fullName, ' по адресу ', user.address, ' на сумму ', totalCost/100) as orders FROM `order`, user WHERE user.id=idUser;
 


 SELECT * FROM product WHERE price BETWEEN 500000 AND 2500000;
 SELECT * FROM product WHERE description LIKE 'Черный%';
 SELECT * FROM product WHERE UPPER (description) LIKE UPPER('%черный%');
 SELECT * FROM product WHERE LOWER (description) LIKE LOWER ('%двухкамерный%');
 
 SELECT * FROM product WHERE idBrand IN (1, 3); 
 DELETE FROM brand WHERE id=2;
 SELECT *, UPPER(name) as name FROM product WHERE idBrand IS NULL;
 SELECT *, LOWER(name) as name FROM product WHERE idBrand IS NOT NULL;
 SELECT DISTINCT idUser FROM `order` WHERE EXISTS (SELECT * FROM product_order WHERE idProduct=24 AND `order`.idUser=product_order.idUser);
 
 
 
  SELECT * FROM `order` ORDER BY totalCost DESC LIMIT 3; #тройка самых дорогих заказов
  SELECT * FROM `order` ORDER BY totalCost ASC LIMIT 3; #тройка самых дeшевых заказов


 SELECT COUNT(DISTINCT idUser) FROM `order`; #колличество польз. делающих заказы
 SELECT MIN(PRICE) FROM product;
 SELECT MAX(PRICE) FROM product;
 SELECT AVG(PRICE) FROM product;
 SELECT SUM(totalCost) FROM `order`;
 
 
 SELECT name, MAX(price) as maxPriceCategory FROM product GROUP BY idCategory; #макс цена в категории
 SELECT name, MIN(price) as minPriceCategory FROM product GROUP BY idCategory;
 SELECT name, AVG(price) as avgPriceCategory FROM product GROUP BY idCategory;
 SELECT idUser, SUM(totalCost) FROM  `order`, user  GROUP BY idUser HAVING SUM(totalCost)>5000000; #пользователи которые купили больше чем на 50т.р.
 SELECT * FROM `order` ORDER BY totalCost DESC LIMIT 3; #тройка самых дорогих заказов
 
 
 
 select * from brand;
 
