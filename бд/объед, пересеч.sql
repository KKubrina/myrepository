DROP DATABASE IF EXISTS shop;
CREATE DATABASE `shop`;
USE `shop`;

SET character_set_server = utf8;
SET character_set_client = utf8;
SET character_set_results = utf8;
SET character_set_connection = utf8;

CREATE TABLE user (
  id INT(11) NOT NULL AUTO_INCREMENT,
  fullName VARCHAR(50) NOT NULL,
  login VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  email VARCHAR(50) NOT NULL,
  address VARCHAR(50) NOT NULL,
  phoneNumber VARCHAR(50) NOT NULL,
  UNIQUE KEY login (login),
  UNIQUE KEY phoneNumber (phoneNumber),
  PRIMARY KEY(id)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;


    CREATE TABLE brand (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  country VARCHAR(50) NOT NULL,
  PRIMARY KEY(id)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;

  CREATE TABLE category (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY(id)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;

    CREATE TABLE discountCard (
  cardNumber INT(11) NOT NULL AUTO_INCREMENT,
  validity VARCHAR(50) NOT NULL,
  idUser INT(11) NOT NULL,
  amountDiscount INT(11) NOT NULL,
  FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE,
  PRIMARY KEY(cardNumber)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;


  CREATE TABLE product (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  description VARCHAR(50) NOT NULL,
  price INT NOT NULL,
  idCategory INT(11),
  idBrand INT(11),
  FOREIGN KEY (idCategory) REFERENCES category (id) ON DELETE SET NULL,
  FOREIGN KEY (idBrand) REFERENCES brand (id) ON DELETE SET NULL,
  PRIMARY KEY(id)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;



  CREATE TABLE review (
  idUser INT(11) NOT NULL,
  idProduct INT(11) NOT NULL,
  text VARCHAR(50) NOT NULL,
  data VARCHAR(50) NOT NULL,
  FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE,
  FOREIGN KEY (idProduct) REFERENCES product (id) ON DELETE CASCADE
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;



   CREATE TABLE `order` (
   id INT(11) NOT NULL AUTO_INCREMENT,
   idUser INT(11) NOT NULL,
   amountProducts INT(11) NOT NULL,
   orderDate VARCHAR(50) NOT NULL,
   totalCost INT(11) NOT NULL, 
   FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE,
   PRIMARY KEY(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;


 CREATE TABLE product_order (
   idProduct INT(11) NOT NULL,
   idUser INT(11) NOT NULL,
   amountProduct INT(11) NOT NULL,
   FOREIGN KEY (idProduct) REFERENCES product (id) ON DELETE CASCADE,
   FOREIGN KEY (idUser) REFERENCES user (id) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8;

 INSERT user VALUES (11, 'Иванов Иван', 'Ivanov_I', '123ivan', 'Ivanov@mail.ru', 'Пушкина, д. 2, кв. 8', '888912');
 INSERT user VALUES (12, 'Иванова Ольга', 'Ivanova_Olga', '11ivanova', 'Ivanova@mail.ru', 'Пушкина, д. 2, кв. 8', '888913');
 INSERT user VALUES (13, 'Смирнова Виктория', 'VikaSm', 'vika001', 'Viktoria@mail.ru', 'Лермантова, д. 12, кв. 31', '313921');
 INSERT user VALUES (14, 'Смирнов Виктор', 'VikTorS', '0viktor001', 'Viktor@mail.ru', 'Омская, д. 141, кв. 19', '273916');
 
 INSERT brand VALUES (1, 'LG', 'Южная Корея');
 INSERT brand VALUES (2, 'Sumsung', 'Южная Корея');
 INSERT brand VALUES (3, 'Apple', 'USA');
 
 INSERT category VALUES (2, 'холодильники');
 INSERT category VALUES (3, 'телефоны');
 
 INSERT product VALUES (20, 'Холодильник LG  HS761', 'Белый двухкамерный холодильник', 3200000, 2, 1);
 INSERT product VALUES (21, 'Холодильник LG  HS31', 'Белый однокамерный холодильник', 1600000, 2, 1);
 INSERT product VALUES (22, 'Холодильник Sumsung  RB36T774FB1', 'Двухкамерный серый холодильник', 4100000, 2, 2);
 INSERT product VALUES (23, 'Телефон Sumsung Galaxy S6', 'Сенсорный черный телефон', 1100000, 3, 2);
 INSERT product VALUES (24, 'Телефон IPhone 7', 'Черный телефон', 2100000, 3, 3);
 
 INSERT `order` VALUES (31, 11, 2, '01.02.2021', 4300000);
 INSERT product_order VALUES (21, 11, 1);
 INSERT product_order VALUES (23, 11, 1);
 INSERT `order` VALUES (32, 12, 2, '02.02.2021', 6200000);
 INSERT product_order VALUES (22, 12, 1);
 INSERT product_order VALUES (24, 12, 1);
 INSERT `order` VALUES (33, 13, 1, '12.01.2021', 2100000);
 INSERT product_order VALUES (23, 13, 1);
 INSERT product_order VALUES (24, 13, 1);
 INSERT `order` VALUES (34, 13, 1, '12.01.2021', 3200000);
 INSERT product_order VALUES (21, 13, 1);
 
#EXEPT
 SELECT * FROM product WHERE idCategory = 2 and
 id NOT IN
 (SELECT id FROM product WHERE idBrand = 2);  
 
 #INTERSECT
 SELECT * FROM product WHERE idCategory = 2 and
 id IN
 (SELECT id FROM product WHERE idBrand = 2);
 
 #UNION
 SELECT * FROM product WHERE idCategory = 2 
 union
 (SELECT * FROM product WHERE idBrand = 2);


SELECT DISTINCT user.fullName
  FROM product_order, user
  WHERE product_order.idUser = user.id and NOT EXISTS
    (SELECT *
        FROM product
        WHERE idCategory=3 and NOT EXISTS
          (SELECT *
              FROM product_order as t1
              WHERE
                  t1.idUser = product_order.idUser and
                  t1.idProduct = product.id));  #кто покупал все телефоны
                  

SELECT product.*, product_order.*, `order`.*, user.*  #эквисоединение
FROM product, product_order, `order`, user 
WHERE product.id = product_order.idProduct and
user.id = product_order.idUser and
`order`.idUser = user.id;

SELECT product.name, product.id, product_order.amountProduct, `order`.orderDate, user.fullName, user.id  #естественное соединение
FROM product, product_order, `order`, user 
WHERE product.id = product_order.idProduct and
user.id = product_order.idUser and
`order`.idUser = user.id;

SELECT product.name, product_order.amountProduct, `order`.orderDate, user.fullName   #композиция
FROM product, product_order, `order`, user 
WHERE product.id = product_order.idProduct and
user.id = product_order.idUser and
`order`.idUser = user.id;


#JOIN
SELECT product.name, product_order.amountProduct, `order`.orderDate, user.fullName 
FROM product
JOIN product_order ON product.id = product_order.idProduct
JOIN user ON user.id = product_order.idUser
JOIN `order` USING (idUser);


SELECT * FROM user INNER JOIN `order` ON `order`.idUser = user.id;

SELECT * FROM user LEFT JOIN `order` ON `order`.idUser = user.id; #внешнее

SELECT * FROM user RIGHT JOIN `order` ON `order`.idUser = user.id;


SELECT * FROM product, `order` WHERE price > totalCost; #тета

SELECT * FROM `order`, `order` copy WHERE `order`.id <> copy.id AND `order`.orderDate=copy.orderDate; 

SELECT DISTINCT `order`.*  FROM `order`, `order` copy WHERE `order`.idUser = copy.idUser; 