DROP DATABASE IF EXISTS bank;
CREATE DATABASE `bank`;
USE `bank`;

CREATE TABLE CLIENTS (
  ID INT(10) NOT NULL AUTO_INCREMENT,
  NAME VARCHAR(1000) NOT NULL,
  PLACE_OF_BIRTH VARCHAR(1000) NOT NULL,
  DATE_OF_BIRTH DATE NOT NULL,
  ADDRESS VARCHAR(1000) NOT NULL,
  PASSPORT VARCHAR(100) NOT NULL,
  PRIMARY KEY(ID)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;
    
  CREATE TABLE TARIFS (
  ID INT(10) NOT NULL AUTO_INCREMENT,
  NAME VARCHAR(1000) NOT NULL,
  COST REAL NOT NULL,
  PRIMARY KEY(ID)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;
  
      CREATE TABLE PRODUCT_TYPE (
  ID INT(10) NOT NULL AUTO_INCREMENT,
  NAME VARCHAR(100) NOT NULL,
  BEGIN_DATE DATE NOT NULL,
  END_DATE DATE, 
  TARIF_REF INT(10) NOT NULL, 
  FOREIGN KEY (TARIF_REF) REFERENCES TARIFS (ID) ON DELETE CASCADE,
  PRIMARY KEY(ID)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;
  
  
      CREATE TABLE PRODUCTS (
  ID INT(10) NOT NULL AUTO_INCREMENT,
  PRODUCT_TYPE_ID INT(10) NOT NULL,
  NAME VARCHAR(100) NOT NULL,
  CLIENT_REF INT(10) NOT NULL,
  OPEN_DATE DATE NOT NULL,
  CLOSE_DATE DATE,  
  FOREIGN KEY (CLIENT_REF) REFERENCES CLIENTS (ID) ON DELETE CASCADE,
  FOREIGN KEY (PRODUCT_TYPE_ID) REFERENCES PRODUCT_TYPE (ID) ON DELETE CASCADE,
  PRIMARY KEY(ID)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;

  CREATE TABLE ACCOUNTS (
  ID INT(10) NOT NULL AUTO_INCREMENT,
  NAME VARCHAR(100) NOT NULL,
  SALDO REAL NOT NULL,
  CLIENT_REF INT(10) NOT NULL,
  OPEN_DATE DATE NOT NULL,  
  CLOSE_DATE DATE,  
  PRODUCT_REF INT(10) NOT NULL,  
  ACC_NUM VARCHAR(25) NOT NULL,  
  FOREIGN KEY (CLIENT_REF) REFERENCES CLIENTS (ID) ON DELETE CASCADE,
  FOREIGN KEY (PRODUCT_REF) REFERENCES PRODUCTS (ID) ON DELETE CASCADE,
  PRIMARY KEY(ID)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;
  
    CREATE TABLE RECORDS (
  ID INT(10) NOT NULL AUTO_INCREMENT,
  DT INT(1) NOT NULL,
  SUM REAL NOT NULL,
  ACC_REF INT(10) NOT NULL,
  OPER_DATE DATE NOT NULL,  
  FOREIGN KEY (ACC_REF) REFERENCES ACCOUNTS (ID) ON DELETE CASCADE,
  PRIMARY KEY(ID)
  ) ENGINE=INNODB DEFAULT CHARSET=utf8;
  
  
insert into tarifs values (1,'Тариф за выдачу кредита', 10);
insert into tarifs values (2,'Тариф за открытие счета', 10);
insert into tarifs values (3,'Тариф за обслуживание карты', 10);

insert into PRODUCT_TYPE values (1, 'КРЕДИТ', '2018-01-01', null, 1);
insert into PRODUCT_TYPE values (2, 'ДЕПОЗИТ', '2018-01-01', null, 2);
insert into PRODUCT_TYPE values (3, 'КАРТА', '2018-01-01', null, 3);

insert into clients values (1, 'Сидоров Иван Петрович', 'Россия, Московская облать, г. Пушкин', '2001-01-01', 'Россия, Московская облать, г. Пушкин, ул. Грибоедова, д. 5', '2222 555555, выдан ОВД г. Пушкин, 10.01.2015');
insert into clients values (2, 'Иванов Петр Сидорович', 'Россия, Московская облать, г. Клин', '2001-01-01', 'Россия, Московская облать, г. Клин, ул. Мясникова, д. 3', '4444 666666, выдан ОВД г. Клин, 10.01.2015');
insert into clients values (3, 'Петров Сидр Иванович', 'Россия, Московская облать, г. Балашиха', '2001-01-01', 'Россия, Московская облать, г. Балашиха, ул. Пушкина, д. 7', '4444 666666, выдан ОВД г. Клин, 10.01.2015');

insert into products values (1, 1, 'Кредитный договор с Сидоровым И.П.', 1, '2015-06-01', null);
insert into products values (2, 2, 'Депозитный договор с Сидоровым И.П.', 1, '2017-08-01', null);
insert into products values (3, 3, 'Карточный договор с Сидоровым И.П.', 1, '2017-08-01', null);


insert into accounts values (1, 'Кредитный счет для Сидорова И.П.', -2000, 1, '2015-06-01', null, 1, '45502810401020000022');
insert into accounts values (2, 'Депозитный счет для Сидорова И.П.', 6000, 1, '2017-08-01', null, 2, '42301810400000000001');
insert into accounts values (3, 'Карточный счет для Сидорова И.П.', 8000, 1, '2017-08-01', null, 3, '40817810700000000001');

insert into records values (1, 1, 5000, 1, '2015-06-01');
insert into records values (2, 0, 1000, 1, '2015-07-01');
insert into records values (3, 0, 2000, 1, '2015-08-01');
insert into records values (4, 0, 3000, 1, '2015-09-01');
insert into records values (5, 1, 5000, 1, '2015-10-01');
insert into records values (6, 0, 3000, 1, '2015-10-01');

insert into records values (7, 0, 10000, 2, '2017-08-01');
insert into records values (8, 1, 1000, 2, '2017-08-05');
insert into records values (9, 1, 2000, 2, '2017-09-21');
insert into records values (10, 1, 5000, 2, '2017-10-24');
insert into records values (11, 0, 6000, 2, '2017-11-26');

insert into records values (12, 0, 120000, 3, '2017-09-08');
insert into records values (13, 1, 1000, 3, '2017-10-05');
insert into records values (14, 1, 2000, 3, '2017-10-21');
insert into records values (15, 1, 5000, 3, '2017-10-24');

insert into products values (4, 2, 'Депозитный договор с Ивановым П.С.', 2, '2019-01-01', null);
insert into accounts values (4, 'Депозитный счет для Иванов П.С.', 0, 2, '2019-01-01', null, 4, '42101810400000000004');
insert into products values (5, 1, 'Кредитный договор с Ивановым П.С.', 2, '2019-01-01', null);
insert into accounts values (5, 'Кредитный счет для Иванов П.С.', 0, 2, '2019-01-01', null, 5, '42101810400000000006');
insert into products values (6, 2, 'Депозитный договор с Петровым С.И.', 3, '2021-04-11', null);
insert into accounts values (6, 'Депозитный счет для Петров С.И.', 0, 3, '2021-04-11', null, 6, '42101240400000020001');

#Сформируйте отчет, который содержит все счета, относящиеся к продуктам типа ДЕПОЗИТ,
#принадлежащих клиентам, у которых нет открытых продуктов типа КРЕДИТ.
SELECT * from accounts WHERE 
CLIENT_REF NOT IN (SELECT CLIENT_REF from PRODUCTS where PRODUCT_TYPE_ID = 1)
AND 
CLIENT_REF in (SELECT CLIENT_REF from PRODUCTS where PRODUCT_TYPE_ID = 2 ); 

#Сформируйте выборку, который содержит средние движения по счетам в рамках одного
#дня, в разрезе типа продукта.
insert into records values (16, 0, 5000, 4, '2021-09-11');
insert into records values (17, 0, 1000, 4, '2021-09-11');
insert into records values (18, 0, 2000, 6, '2021-09-11');
insert into records values (19, 1, 1000, 6, '2021-09-11');
#PRODUCT_TYPE_ID = 2 (ДЕБЕТ)
SELECT AVG(SUM) from records where OPER_DATE = '2021-09-11' and ACC_REF in (SELECT accounts.id from accounts, products WHERE accounts.PRODUCT_REF = products.id and products.PRODUCT_TYPE_ID = 2);

insert into records values (20, 1, 1000, 1, '2021-09-11');
insert into records values (21, 1, 4000, 5, '2021-09-11');
insert into records values (22, 0, 1000, 5, '2021-09-11');
#PRODUCT_TYPE_ID = 1 (КРЕДИТ)
SELECT AVG(SUM) from records where OPER_DATE = '2021-09-11' and ACC_REF in (SELECT accounts.id from accounts, products WHERE accounts.PRODUCT_REF = products.id and products.PRODUCT_TYPE_ID = 1);

#Сформируйте выборку, в который попадут клиенты, у которых были операции по счетам за
#прошедший месяц от текущей даты. Выведите клиента и сумму операций за день в
#разрезе даты.
SELECT clients.*, SUM(sum) from records, products, clients WHERE 
ACC_REF = products.id 
AND 
CLIENT_REF = clients.id 
AND  
OPER_DATE between DATE_SUB(CURDATE(), INTERVAL 1 MONTH) and CURDATE() 
group by clients.id;

delimiter \
CREATE PROCEDURE normalization()
BEGIN
DECLARE done INT DEFAULT 0;
DECLARE actualSum DOUBLE;
DECLARE id_acc INT(11);
DECLARE ref_acc INT(11);
DECLARE rec_sum DOUBLE;
DECLARE rec_dt INT(11);
DECLARE oldSum DOUBLE;
DECLARE records_cur CURSOR for SELECT ACC_REF, SUM, DT FROM records ORDER BY ACC_REF;
DECLARE accounts_cur CURSOR for SELECT SALDO, ID FROM accounts ORDER BY ID;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

 
 OPEN accounts_cur; 
 OPEN records_cur; 
 set ref_acc = 1;

 REPEAT
   FETCH accounts_cur INTO oldSum, id_acc; 
   set actualSum = 0;
   set rec_sum = 0;

   IF id_acc = ref_acc THEN
   
   REPEAT 
   set actualSum = actualSum + rec_sum;
   FETCH records_cur INTO ref_acc, rec_sum, rec_dt;
   if rec_dt = 1 then 
   set rec_sum = 0 - rec_sum;
   end if;
   select ref_acc;
   UNTIL id_acc+1 = ref_acc END REPEAT;

   
   if actualSum <> oldSum THEN
   UPDATE ACCOUNTS
   SET SALDO = actualSum
   WHERE ID = id_acc;
        END IF;

   END IF;
 
 UNTIL done END REPEAT;
 CLOSE accounts_cur; 
 CLOSE records_cur; 
 END; 
\




delimiter ;

select * from accounts;
call normalization();
select * from accounts;
select * from records ORDER BY ACC_REF;