DROP DATABASE IF EXISTS task22;
CREATE DATABASE `task22`;
USE `task22`;


CREATE TABLE team_trainer (
  nameTeam VARCHAR(50) NOT NULL,
  nameTrainer VARCHAR(50) NOT NULL
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE genre_writer (
  genre VARCHAR(50) NOT NULL,
  writer VARCHAR(50) NOT NULL
) ENGINE=INNODB DEFAULT CHARSET=utf8;

 CREATE TABLE competition (
  nameTeam VARCHAR(50) NOT NULL,
  nameTrainer VARCHAR(50) NOT NULL,
  points INT(11) NOT NULL,
  place INT(11) NOT NULL
) ENGINE=INNODB DEFAULT CHARSET=utf8;


INSERT team_trainer VALUES ('Красные', 'Иванов Иван');
INSERT team_trainer VALUES ('Синие', 'Петров Петр');
INSERT team_trainer VALUES ('Белые', 'Олегов Олег');
INSERT team_trainer VALUES ('Белые', 'Иванов Иван');
INSERT team_trainer VALUES ('Красные', 'Алексеев Алексей');
INSERT team_trainer VALUES ('Синие', 'Петров Петр');

INSERT genre_writer VALUES ('Фантастика', 'Булгаков М А');
INSERT genre_writer VALUES ('Роман', 'Достоевский Ф М');
INSERT genre_writer VALUES ('Детектив', 'Булгаков М А');
INSERT genre_writer VALUES ('Роман', 'Булгаков М А');
INSERT genre_writer VALUES ('Детектив', 'Достоевский Ф М');


INSERT competition VALUES ('Красные', 'Иванов Иван', 10, 1);
INSERT competition VALUES ('Синие', 'Петров Петр', 9, 2);
INSERT competition VALUES ('Белые', 'Олегов Олег', 8, 3);
INSERT competition VALUES ('Белые', 'Иванов Иван', 9, 2);
INSERT competition VALUES ('Красные', 'Алексеев Алексей', 10, 1);
INSERT competition VALUES ('Синие', 'Петров Петр', 7, 3);

# команды, которые тренировал более чем один тренер
SELECT DISTINCT t_t1.nameTeam FROM team_trainer as t_t1
WHERE EXISTS (SELECT team_trainer.nameTrainer FROM team_trainer 
WHERE team_trainer.nameTeam = t_t1.nameTeam 
and team_trainer.nameTrainer <> t_t1.nameTrainer ); 


# писателей, которые писали во всех жанрах, представленных в таблице
SELECT DISTINCT writer
  FROM genre_writer as t1 
  WHERE NOT EXISTS 
    (SELECT writer
        FROM genre_writer as t2
        WHERE NOT EXISTS
          (SELECT *
              FROM genre_writer 
              WHERE
                   t1.writer = genre_writer.writer 
                  and t2.genre = genre_writer.genre ));

# жанры, в которых писали все
SELECT DISTINCT genre
  FROM genre_writer as t1 
  WHERE NOT EXISTS 
    (SELECT genre
        FROM genre_writer as t2
        WHERE NOT EXISTS
          (SELECT *
              FROM genre_writer 
              WHERE
                   t1.genre = genre_writer.genre 
                  and t2.writer = genre_writer.writer ));
                  
                  
                  
#тренеров, которые не тренировали заданную команду
SELECT DISTINCT nameTrainer FROM team_trainer WHERE nameTrainer NOT IN 
(SELECT nameTrainer FROM team_trainer
 WHERE nameTeam = 'Белые'); 

#тренеров, для которых среднее количество очков команд, которые они тренировали, больше среднего значения по всем тренерам из таблицы
SELECT DISTINCT nameTrainer, AVG(points) FROM competition
GROUP BY nameTrainer 
ORDER BY AVG(points) DESC LIMIT 1; 

#команды, становившиеся чемпионами с разными тренерами

SELECT DISTINCT c1.nameTeam FROM competition as c1
WHERE EXISTS (SELECT competition.nameTeam FROM competition 
WHERE competition.nameTrainer <> c1.nameTrainer 
AND competition.nameTeam=c1.nameTeam 
AND competition.place = 1
AND c1.place = 1); 

#команды, которые тренировали тренеры, выигравшие чемпионат не с этой, а с другими командами
SELECT nameTeam FROM competition WHERE nameTrainer in 
(SELECT nameTrainer FROM competition WHERE place = 1) 
AND place <> 1;