#pragma once
#pragma once
#include <vector>
#include <iostream>
#include "Box.h"

namespace ConBox {


	class Container {
	private:
		std::vector<Box> arrayBox;
		int lenght;
		int width;
		int height;
		double maxWeight;

	public:

		Container() {
			this->lenght = 0;
			this->width = 0;
			this->height = 0;
			this->maxWeight = 0;
		}

		Container(int l, int w, int h, double mW) {
			this->lenght = l;
			this->width = w;
			this->height = h;
			this->maxWeight = mW;
		}

		Box& operator[] (const int index)
		{

			return arrayBox[index];
		}

		int addBoxWithPam(ConBox::Box x) {
			double w = 0;
			for (int i = 0; i < arrayBox.size(); i++) {
				w += arrayBox[i].getWeight();
			}
			try {
				if ((x.getWeight() + w) <= maxWeight) {
					arrayBox.push_back(x);
					return arrayBox.size();
				}
				else {
					throw - 1;
				}
			}

			catch (int i)
			{
				std::cout << "Error #" << i << "\n";
			}
		}



		void deleteBox(int index) {
			arrayBox.erase(arrayBox.begin() + index);
		}

		void addBox(int index, ConBox::Box x) {
			arrayBox.insert(arrayBox.begin() + index, x);
		}

		int amountOfBox() {
			return arrayBox.size();
		}

		double weightAll() {
			double w = 0.0;
			for (int i = 0; i < arrayBox.size(); i++) {
				w += arrayBox[i].getWeight();
			}
			return w;
		}

		int valueAll() {
			int w = 0.0;
			for (int i = 0; i < arrayBox.size(); i++) {
				w += arrayBox[i].getValue();
			}
			return w;
		}

		Box getBox(int i) {
			return arrayBox[i];
		}


		friend std::ostream& operator<< (std::ostream& out, const Container& a);
		friend std::istream& operator>> (std::istream& in, Container& point);
	};

}

