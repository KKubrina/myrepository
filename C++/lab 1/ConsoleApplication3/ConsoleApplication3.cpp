﻿// ConsoleApplication3.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//


#include <iostream>
#include "Box.h"
#include "Container.h"

int main()
{
	
	ConBox::Box box01 = ConBox::Box(1, 1, 1, 1, 1);
	ConBox::Box box02 = ConBox::Box(2, 2, 2, 2, 2);
	ConBox::Box box03 = ConBox::Box(3, 3, 3, 3, 3);

	ConBox::Container container = ConBox::Container(100, 110, 120, 200);
	container.addBox(0, box01); //добавляем коробку в контейнер
	container.addBox(1, box03); //добавляем коробку в контейнер
	container.addBox(2, box02); //добавляем коробку в контейнер
	//std::cout << "Amount of box " << container.amountOfBox() << '\n'; //колличество коробок
	//std::cout << "Weight all boxes " << container.weightAll() << '\n'; //Вес всех коробок
	//std::cout << "Value all boxes " << container.valueAll() << '\n'; //цена всех коробок

	//container.deleteBox(1); //удаляем вторую коробку(box03) из контейнера
    //int b = container.addBoxWithPam(box03); //добавляем коробку (box03), проверяя что суммарный вес коробок внутри позволяет добавить эту
	//std::cout <<"Value box[1]: " << container.getBox(1).getValue() << '\n'; //коробка (box02), которая была третьей в контейнере, теперьв вторая
	//container[1] = ConBox::Box(2, 2, 2, 2, 4); //меняем у второй коробки цену
	//std::cout << "Value box[1]: " << container.getBox(1).getValue() << '\n'; //проверяем 

	std::cin >> container; //ввод коробки
	std::cout << "container : " << container << '\n'; //вывод коробки

	return 0;
}


