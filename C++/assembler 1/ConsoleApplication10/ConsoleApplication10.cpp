﻿// ConsoleApplication10.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <windows.h>


const void show(const char* line, int sizeLine) {
    char fmt[] = "%c";
    __asm {
        mov ebx, line
        mov ecx, sizeLine

        cmp ecx, 0
        je FIN

    CYCLE :

        push ebx
        push ecx
        mov eax, [ebx]
        push eax
        lea eax, fmt
        push eax
        call byte ptr printf
        add esp, 8
        pop ecx
        pop ebx
        inc ebx

        loop CYCLE

    FIN:
    }
}


void enterStr(char* str) {

    char fmt[] = "%c";
    __asm {
        mov esi, 0   
        mov ebx, str  
        lea eax, [ebx + 1 * esi]
        push eax
        lea eax, fmt 
        push eax
        call byte ptr scanf 
        add esp, 8

        WHILE:

        lea eax, [ebx + 1 * esi]

            cmp[eax], '\n'
            je NEXT

            inc esi
            lea eax, [ebx + 1 * esi]
            push eax
            lea eax, fmt 
            push eax
            call byte ptr scanf 
            add esp, 8

            jmp WHILE

            NEXT:
    }
}

void enterInt(int* a) {

    char fmt[] = "%d";
    __asm {
        mov ebx, a  
        lea eax, [ebx]
        push eax
        lea eax, fmt 
        push eax
        call dword ptr scanf
        add esp, 8
      
    }
}

char* podstr(const char* str, int pos, int* len) {
    char* ans;
    __asm {

        mov ebx, str
        xor edi, edi

        WHILE :

        lea eax, [ebx + 1 * edi]

            cmp[eax], '\n'
            je NEXT

            inc edi

            jmp WHILE

            NEXT :

        mov eax, pos
            cmp eax, edi
            jg FINLEN

            mov edx, [len]
            add eax, [edx]
            cmp eax, edi
            jg EDIT_LEN

            DONE_EDIT_LEN :

        mov eax, 1
            push eax
            call dword ptr malloc
            add esp, 4
            mov ans, eax

            cmp eax, 0
            je FIN



            mov edx, str
            add edx, pos
            xor esi, esi
            mov ebx, [len]

            FOR :

        cmp esi, [ebx]
            jge FIN
            mov ch, [edx][esi]
            mov byte ptr[eax][esi], ch
            inc esi
            jmp FOR

            EDIT_LEN :
        mov eax, len
            mov ebx, edi
            sub ebx, pos
            mov[eax], ebx
            jmp DONE_EDIT_LEN

            FINLEN :
        mov eax, len
            mov[eax], dword ptr 0

            FIN :
    }
    return ans;
}

double function(double x, double epsilon = 1e-5) {
    double answer; 
    double check;
  
    int i = 0;
    __asm {
        finit
        fldz
        lea esi, i

        CYCLE:

           fld1
           fchs
           mov eax, [esi]
           mov edx, 0
           mov ebx, 2
           div ebx
           cmp edx, 0
           jne NEXT
           fabs

               NEXT:

           mov eax, 2
           mul [esi]
           xor edi, edi
               fld qword ptr x
               fld st(0)
               FOR:
           cmp edi, eax
               jge NEXT1
               fmul st(1), st(0)
               inc edi
               jmp FOR
               

               NEXT1:
           fstp st(0)
               fst check
               fmulp st(1), st(0)
               fild dword ptr i
               fld1
               fadd st(0), st(0)
               fmulp st(1), st(0)
               fld1
               faddp st(1), st(0)
               fld st(0)

               WHILE:
               fld1
               fcomip st(0), st(1)
               je NEXT2
               fld1
               fsubp st(1), st(0)
               fmul st(1), st(0)
               jmp WHILE

                   NEXT2:
               fstp st(0)
                   fst check
                   fdivp st(1), st(0)
                   fadd st(1), st(0) ;сумма ряда
                   fabs
           fld qword ptr epsilon
           fcomip st(0), st(1)
               ja FIN
               fstp check
               fst answer
               inc [esi]
               jmp CYCLE


               FIN:
           fstp check
           fstp answer
    }
    return answer;
}



void main()
{
  /* int answer = 1;
    do {
        show("Enter the string: ", 19);
        char* array = new char[4];
        enterStr(array);
        show("Enter the position: ", 21);
        int pos;
        enterInt(&pos);
        show("Enter the lenght: ", 19);
        int len;
        enterInt(&len);
        podstr(array, pos, &len);
        show(podstr(array, pos, &len), len);
        show("\nPress 1 to continue or 0 to exit: ", 37);
        enterInt(&answer);
        show("\n", 2);
        enterStr(array); //для enter
    } while (answer == 1);*/
     std::cout << function(0.523598775598298);
}
