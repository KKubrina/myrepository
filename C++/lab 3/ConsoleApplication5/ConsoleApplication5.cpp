﻿
#include <iostream>
#include "Queue.h"
#include "Iterator.h"

int main()
{
    int one, two;
    Queue q = Queue(4);
    one = q.takeFirst(); //error 0
    q.pushBack(1);
    q.pushBack(2);
    q.pushBack(3);
    q.pushBack(4);
    q.pushBack(5); //error 1
    std::cout << "Size of queue: " << q.sizeQueue() << '\n'; 


    //вывод элементов при помощи итератора
    std::cout << "queue:\n";
    Iterator iter = Iterator(q);
    iter.start();
    while (iter.finish() != 1) {
        std::cout << iter.getValue() << ' ';
        iter.next();
    }
    std::cout << '\n';


    one = q.takeFirst();
    std::cout << "First element was: " << one << '\n';
    std::cout << "Size of queue: " << q.sizeQueue() << '\n';
    q.pushBack(8);


    //вывод элементов при помощи итератора
    std::cout << "queue:\n";
    Iterator iter1 = Iterator(q);
    iter1.start();
    while (iter1.finish() != 1) {
        std::cout << iter1.getValue() << ' ';
        iter1.next();
    }
    std::cout << '\n';


    two = q.getFirst();
    std::cout << "First element now: " << two << '\n';
    std::cout << "Size of queue: " << q.sizeQueue() << '\n';


    std::cout << "Queue is empty: " << q.checkEmpty() << '\n';
    q.doEmpty();
    std::cout << "Queue is empty: " << q.checkEmpty() << '\n';

    //вывод элементов при помощи итератора
    std::cout << "queue:\n";
    Iterator iter2 = Iterator(q);
    iter2.start();
    while (iter2.finish() != 1) {
        std::cout << iter2.getValue() << ' ';
        iter2.next();
    }
    std::cout << '\n';

 
}

