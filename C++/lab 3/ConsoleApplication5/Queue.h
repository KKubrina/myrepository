#pragma once
#include <iostream>

class Queue {

public:

    int* array;
    int last;
    const int size;

    Queue() : last(0), size(0)
    {
        array = new int[0];
    }

    Queue(int s) : last(0), size(s) {
        array = new int[s];
    }

    void pushBack(int a) {
        try {
            if (last + 1 <= size) {
                array[last] = a;
                last += 1;
            }
            else {
                throw - 0;
            }
        }
        catch (int i)
        {
            std::cout << "Error #" << i << "\n";
        }
    }

    int takeFirst() {
        try {
            if (last > 0) {
                int a = array[0];
                for (int i = 0; i < last; i++) {
                    array[i] = array[i + 1];
                }
                last -= 1;
                return a;
            }
            else {
                throw - 1;
            }
        }
        catch (int i)
        {
            std::cout << "Error #" << i << "\n";
        }
    }

    int getFirst() {
        try {
            if (last > 0) {

                return array[0];
            }
            else {
                throw - 1;
            }
        }
        catch (int i)
        {
            std::cout << "Error #" << i << "\n";
        }
    }

    int sizeQueue() {
        return last;
    }

    void doEmpty() {
        if (last > 0) {
            delete[] array;
            array = new int[size];
            last = 0;
        }
    }

    bool checkEmpty() {
        if (last > 0) { return false; }
        else { return true; }
    }
};
