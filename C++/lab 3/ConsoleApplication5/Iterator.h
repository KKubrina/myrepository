#pragma once
#include <iostream>
#include "Queue.h"

class Iterator {
private:
    Queue q;
    int pointer;

public:
    Iterator(Queue a) : q(a) {
    }

    void start() {
        try {
            if (q.sizeQueue() > 0) {
                pointer = 0;
            }
            else {
                throw 2;
            }
        }
        catch (int i)
        {
            std::cout << "Error #" << i << "\n";
        }
    }

    void next() {
        try {
            if (q.sizeQueue() != pointer) {
                pointer += 1;
            }
            else {
                throw 3;
            }
        }
        catch (int i)
        {
            std::cout << "Error #" << i << "\n";
        }
    }

    bool finish() {
        if (pointer == q.sizeQueue()) { return true; }
        else { return false; }
    }

    int getValue() {
        return q.array[pointer];
    }
};
