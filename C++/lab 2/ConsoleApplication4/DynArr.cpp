#include <iostream>
#include "DynArr.h"


bool operator== (const DynamicArrayOfIntegers& a, const DynamicArrayOfIntegers& b)
{
	try {
		if (a.size != b.size) { throw - 1; }
		else {
			for (int i = 0; i < a.size; i++) {
				if (a.array[i] != b.array[i]) { return false; }
			}
			return true;
		}
	}

	catch (int i)
	{
		std::cout << "Error #" << i << "\n";
	}
}

bool operator!= (const DynamicArrayOfIntegers& a, const DynamicArrayOfIntegers& b)
{
	try {
		if (a.size != b.size) { throw - 2; }
		else {
			for (int i = 0; i < a.size; i++) {
				if (a.array[i] != b.array[i]) { return  true; }
			}
			return false;
		
		}
	}

	catch (int i)
	{
		std::cout << "Error #" << i << "\n";
	}
	
}

bool operator> (const DynamicArrayOfIntegers& a, const DynamicArrayOfIntegers& b)
{
	int size;
	if (a.size < b.size) { size = a.size; }
	else { size = b.size; }
	for (int i = 0; i < size; i++) {
		if (a.array[i] < b.array[i]) { return  false; }
		if (a.array[i] > b.array[i]) { return true; }
	}
	return false;
}

bool operator>= (const DynamicArrayOfIntegers& a, const DynamicArrayOfIntegers& b)
{
	int size;
	if (a.size < b.size) { size = a.size; }
	else { size = b.size; }
	for (int i = 0; i < size; i++) {
		if (a.array[i] < b.array[i]) { return  false; }
		if (a.array[i] > b.array[i]) { return true; }
	}
	return true;
}

bool operator< (const DynamicArrayOfIntegers& a, const DynamicArrayOfIntegers& b)
{
	int size;
	if (a.size < b.size) { size = a.size; }
	else { size = b.size; }
	for (int i = 0; i < size; i++) {
		if (a.array[i] > b.array[i]) { return  false; }
		if (a.array[i] < b.array[i]) { return true; }

	}
	return false;
}

bool operator<= (const DynamicArrayOfIntegers& a, const DynamicArrayOfIntegers& b)
{
	int size;
	if (a.size < b.size) { size = a.size; }
	else { size = b.size; }
	for (int i = 0; i < size; i++) {
		if (a.array[i] > b.array[i]) { return  false; }
		if (a.array[i] < b.array[i]) { return true; }
	}
	return true;
}

const DynamicArrayOfIntegers operator+ (const DynamicArrayOfIntegers& a, const DynamicArrayOfIntegers& b) {
	DynamicArrayOfIntegers sum = DynamicArrayOfIntegers(a.size + b.size);
	for (int i = 0; i < a.size; i++) {
		sum.array[i] = a.array[i];
	}
	for (int i = a.size; i < a.size + b.size; i++) {
		sum.array[i] = b.array[i- a.size];
	}
	return DynamicArrayOfIntegers(sum);
}

std::ostream& operator<< (std::ostream& out, const DynamicArrayOfIntegers& a)
{
	try {
		if (a.array!=nullptr) {
			out << "Dynamic Array Of Integers: size - " << a.size << ",\n Array : ";
			for (int i = 0;i < a.size;i++) {
				out << "[" << i << "] = " << a.array[i] << "\n";
			}
			return out;
		}
		else {
			throw - 3;
		}
	}

	catch (int i)
	{
		std::cout << "Error #" << i << "\n";
	}

}
