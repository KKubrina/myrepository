﻿// ConsoleApplication4.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include "DynArr.h"


int main() {
	DynamicArrayOfIntegers dynAr = DynamicArrayOfIntegers(5, 2);
	DynamicArrayOfIntegers dynAr1 = DynamicArrayOfIntegers(5);
	DynamicArrayOfIntegers dynAr2 = DynamicArrayOfIntegers();
	DynamicArrayOfIntegers dynAr3 = DynamicArrayOfIntegers(dynAr);

	std::cout << "Size of dynAr2 : " << dynAr2.sizeOf() << "\n";
	//заполяем массив
	std::cout << "dynAr2 : " << dynAr << "\n\n";
	/*меняем первый эелемент
	dynAr2[0] = 5;
	dynAr2.reserve(5);
	std::cout << "dynAr2 reserve size : " << dynAr2.capasity() << "\n";
	std::cout << "dynAr2 : " << dynAr2 << "\n\n";
	//меняем размер
	dynAr2.resize(3);
	std::cout << "dynAr2 reserve size : " << dynAr2.capasity() << "\n";
	std::cout << "dynAr2 : " << dynAr2 << "\n\n";
	//меняем размер обратно
	dynAr2.resize(5);
	std::cout << "dynAr2 reserve size : " << dynAr2.capasity() << "\n";
	std::cout << "dynAr2 : " << dynAr2 << "\n\n";
	dynAr2.pushBack(4);
	std::cout << "dynAr2 reserve size : " << dynAr2.capasity() << "\n";
	std::cout << "dynAr2 : " << dynAr2 << "\n\n";
	dynAr2.popBack();
	std::cout << "dynAr2 reserve size : " << dynAr2.capasity() << "\n";
	std::cout << "dynAr2 : " << dynAr2 << "\n\n";


	//dynAr = dynAr2;
	//dynAr1 = std::move(dynAr2);
	std::cout << dynAr << dynAr1 << dynAr3 <<"\n\n";
	//std::cout << dynAr2;

//	std::cout << "\n\nChek operators\n\n";
//	if (dynAr == dynAr3) {
//		std::cout << "Dinamic arrays are equal" << "\n";
//	}
//	if (dynAr != dynAr3) {
//		std::cout << "Dinamic arrays are not equal" << "\n";
//	}
//	if (dynAr > dynAr3) {
//		std::cout << "dynAr > dynAr3" << "\n";
//	}
//	if (dynAr >= dynAr3) {
//		std::cout << "dynAr >= dynAr3" << "\n";
//	}
//	if (dynAr < dynAr3) {
//		std::cout << "dynAr < dynAr3" << "\n";
//	}
//	if (dynAr <= dynAr3) {
//		std::cout << "dynAr <= dynAr3" << "\n";
//	}

	std::cout << dynAr1 + dynAr3;
	*/
	return 0;
}