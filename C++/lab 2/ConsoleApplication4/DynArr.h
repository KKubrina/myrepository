#pragma once
#include <iostream>

class DynamicArrayOfIntegers {


private:

	int size;
	int* array;
	int reserv;

public:
	DynamicArrayOfIntegers() : size(0)
	{
		array = new int[0];
	}

	DynamicArrayOfIntegers(int s) : size(s) {
		array = new int[size];
		for (int i = 0; i < size; i++) {
			array[i] = 0;
		}
	}

	DynamicArrayOfIntegers(int s, int n) : size(s) {
		array = new int[size];
		for (int i = 0; i < size; i++) {
			array[i] = n;
		}
	}

	DynamicArrayOfIntegers(int s, int n, int r) : size(s), reserv(r) {
		array = new int[size];
		for (int i = 0; i < size; i++) {
			array[i] = n;
		}
	}


	DynamicArrayOfIntegers(const DynamicArrayOfIntegers& object)
	{
		size = object.size;
		array = new int[size];
		for (int i = 0; i < size; i++) {
			array[i] = object.array[i];
		}
	}

	~DynamicArrayOfIntegers() {
		delete[] array;
	}

	DynamicArrayOfIntegers(DynamicArrayOfIntegers&& object) : size(object.size), array(object.array) {
		object.size = 0;
		delete[] object.array;
		object.array = nullptr;
	}

	int sizeOf() {
		return size;
	}

	int& operator[] (const int index)
	{
		try {
			if (index < size) {
				return array[index];
			}
			else {
				throw - 5;
			}
		}
		catch (int i)
		{
			std::cout << "Error #" << i << "\n";
		}
	}
	
	int capasity() {
		return reserv;
	}

	void resize(int newSize) {
		if (newSize > 0 ) {
			if (size + reserv < newSize) {
				int* array1 = new int[newSize];
				if (size < newSize) {
					for (int i = 0; i < size; i++) {
						array1[i] = array[i];

					}
					for (int i = size; i < newSize; i++) {
						array1[i] = 0;
					}
				}
				delete[] array;
				array = new int[newSize];
				for (int i = 0; i < newSize; i++) {
					array[i] = array1[i];
				}
				delete[] array1;
				size = newSize;
			}

			else if ((size + reserv) >= newSize and size <= newSize) {
				reserv = reserv + size - newSize;
				size = newSize;
			}
			
			else if (size > newSize) {
				reserv += (size - newSize);
				size = newSize;
			}
		}
	}

	void reserve(int n) {
		int s = n + size;
			int* array1 = new int[s];
			for (int i = 0; i < size; i++) {
					array1[i] = array[i];
			}
			for (int i = size; i < s; i++) {
					array1[i] = 0;
			}
			delete[] array;
			array = new int[s];
			for (int i = 0; i < s; i++) {
				array[i] = array1[i];
			}
			delete[] array1;
			reserv = n;
		
	}
	void pushBack(int x) {
		if (reserv > 0) {
			size += 1;
			reserv -= 1;
			array[size - 1] = x;
		}
		else {
			resize(size + 1);
			array[size - 1] = x;
		}
	}

	void popBack() {
		if (reserv > 0) {
			size -= 1;
			reserv += 1;
			array[size] = 0;
		}
		else {
			resize(size - 1);
		}
	}

	DynamicArrayOfIntegers& operator=(const DynamicArrayOfIntegers& x)
	{

		if (&x == this) {
			return *this;
		}
		delete[] array;
		array = new int[x.size];
		for (int i = 0; i < x.size; i++) {
			array[i] = x.array[i];
		}
		size = x.size;
		return *this;
	}

	DynamicArrayOfIntegers& operator=(DynamicArrayOfIntegers&& x) {
		std::swap(array, x.array);
		std::swap(size, x.size);
		x.size = 0;
		delete[] x.array;
		x.array = nullptr;
		return *this;
	}



	friend bool operator== (const DynamicArrayOfIntegers& a, const DynamicArrayOfIntegers& b);
	friend bool operator!= (const DynamicArrayOfIntegers& a, const DynamicArrayOfIntegers& b);
	friend bool operator> (const DynamicArrayOfIntegers& a, const DynamicArrayOfIntegers& b);
	friend bool operator>= (const DynamicArrayOfIntegers& a, const DynamicArrayOfIntegers& b);
	friend bool operator< (const DynamicArrayOfIntegers& a, const DynamicArrayOfIntegers& b);
	friend bool operator<= (const DynamicArrayOfIntegers& a, const DynamicArrayOfIntegers& b);
	friend const DynamicArrayOfIntegers operator+ (const DynamicArrayOfIntegers& a, const DynamicArrayOfIntegers& b);
	friend std::ostream& operator<< (std::ostream& out, const DynamicArrayOfIntegers& a);


};