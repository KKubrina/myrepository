﻿// ConsoleApplication7.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//1 - пустой
//2 - size == 0
//3 - старт итератора не был запущен
//4 - 
//5
//6

#include <iostream>
#include "HashTable.h"

class Iterator {
    ListH** array;
    int size;
    ListH* pointerList;
    int pointer = -1;
    int pointer1 = -1;

public:

    Iterator(HashTable* a) {
        array = a->array;
        size = a->size;
    }

    void start() {
        try {
            if (size != 0) {
                int flag = 0;
                for (int i = 0; i < size; i++) {
                    if (array[i] != nullptr) {
                        flag = 1;
                        pointer = i;
                        pointerList = array[i];
                        break;
                    }
                }
                if (flag == 0) {
                    throw 1;
                }
            
            }

            else { throw 2; }

        }

        catch (int i) {

            std::cout << "Error : " << i << "\n";

        }
    }

    void next() {
        try {
            if (pointer != -1) {
                if (pointerList->next != nullptr) {
                    pointerList = pointerList->next;
                }
                else if (pointerList->next == nullptr) {
                    for (int i = pointer+1; i < size; i++) {
                        if (array[i] != nullptr) {
                            pointer = i;
                            pointerList = array[i];
                            break;
                        }
                        else {
                            pointer = -2;
                        }
                    }
                }
            }
            else { throw 3; }
        }

        catch (int i) {

            std::cout << "Error : " << i << "\n";

        }
    
    }

    TElem getValue() {
        return pointerList->value;
        }
    
    
    bool checkAll() {
        try {
            if (pointer != -1 and size > 0) {
                return (pointer == -2);
            }
             
            else { throw 2; }
        }

        catch (int i) {

            std::cout << "Error : " << i << "\n";

        }

    
    }

};

int main()
{
    

 HashTable* a = new HashTable(10);
 Iterator* b = new Iterator(a);
    a->addElement(5);
    a->addElement(52);
    a->addElement(51);
    a->addElement(50);
    a->addElement(60);
    a->addElement(190);
    a->addElement(800);

    b->start();
    while (!b->checkAll()) {
        std::cout <<b->getValue()  << std::endl;
        b->next();
    }
    std::cout << "\n\n" << std::endl;


    a->deleteElement(51);
    b = new Iterator(a);
    b->start();
    while (!b->checkAll()) {
        std::cout << b->getValue() << std::endl;
        b->next();
    }
    std::cout << "\n\n" << std::endl;


    a->deleteElement(50);
    b = new Iterator(a);
    b->start();
    while (!b->checkAll()) {
        std::cout << b->getValue() << std::endl;
        b->next();
    }
    std::cout << "\n\n" << std::endl;



    a->deleteElement(52);
    b = new Iterator(a);
    b->start();
    while (!b->checkAll()) {
        std::cout << b->getValue() << std::endl;
        b->next();
    }
    std::cout << "\n\n" << std::endl;

  //  a->deleteElement(53);

    std::cout << a->seachElement(190) << std::endl;
    std::cout << a->seachElement(800) << std::endl;
    std::cout << a->seachElement(700) << std::endl;
    std::cout << a->seachElement(1) << std::endl;
    std::cout << a->checkEmpty() << std::endl;
    a->doEmpty();
    std::cout << a->checkEmpty() << std::endl;

    b = new Iterator(a);
    b->start();
    while (!b->checkAll()) {
        std::cout << b->getValue() << std::endl;
        b->next();
    }
    std::cout << "\n\n" << std::endl;



    
//    Iterator* b = new Iterator(a);
}

