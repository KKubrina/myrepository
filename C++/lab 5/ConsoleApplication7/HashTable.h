#pragma once
template <typename TElem>

struct ListH
{
    TElem value;
    ListH* next;
};

class HashTable {
public:
    int size;
    ListH** array;
    const double A = 0.1;


    HashTable(int s)
    {
        array = new ListH*[s];
        for (int i = 0; i < s; i++)
        {
            array[i] = nullptr;
        }

        size = s;

    }

    int hashCode(TElem a) {
        int hc = int((A * a));
        while (hc > size) {
            hc = int((hc / 10));
        }
        return hc;
    }

  


    void addElement(TElem a) {
        int hc = hashCode(a);
        if (array[hc] != nullptr) {
            ListH* l = new ListH;
            l = array[hc];
            while (l != nullptr) {
                if (l->value == a) {
                    l->value = a;
                    break;
                }
                else if (l->value > a) {
                    if (l->next != nullptr) { l = l->next; }
                    else {
                        ListH *l1 = new ListH;
                        l1->next=nullptr;
                        l1->value = a;
                        l->next=l1;
                    }
                }
                else if (l->value < a) {
                    ListH* l1 = new ListH;
                    l1->value = l->value;
                    l1->next = l->next;
                    l->value = a;
                    l->next = l1;
                }
            }
        }
        else {
            array[hc] = new ListH;
            array[hc]->value = a;
            array[hc]->next = nullptr;
        }
    }

    void deleteElement(TElem a) {
        try {
            int hc = hashCode(a);
            if (array[hc] != nullptr) {
                ListH* l = new ListH;
                int i1 = 0;
                l = array[hc];
                if (i1 == 0 and array[hc]->next == nullptr) {
                    array[hc] = nullptr;
                }
                else {
                    int flag = 0;
                    while (l != nullptr) {
                        if (l->value == a) {
                            flag = 1;
                            int sizeI = 0;
                            ListH* l1 = new ListH;
                            l1 = array[hc];
                            while (l1->next != nullptr) {
                                sizeI += 1;
                                l1 = l1->next;
                            }
                            if (l->next != nullptr) {
                                for (int i = i1; i < sizeI - 1; i++) {
                                    l->value = l->next->value;
                                    l = l->next;
                                }
                                l->value = l->next->value;;
                                l->next = nullptr;
                                l = l->next;

                                delete l;
                                break;
                            }
                            else {
                                l1 = array[hc];
                                for (int i = 0; i < sizeI-1; i++) {
                                    l1 = l1->next;
                                }
                                l1->next = nullptr;
                                delete l;
                                break;
                            }

                        }

                        else {
                            if (l->next != nullptr) {
                                l = l->next;
                                i1++;
                            }
                        }
                    }
                    if (flag == 0) { throw 1; }
                }
            }
            else { throw 2; }
            
        }

        catch (int i) {

            std::cout << "Error : " << i << "\n";

        }
    }

    bool seachElement(TElem a) {
        try {
            int hc = hashCode(a);
            if (array[hc] != nullptr) {
                ListH* l = new ListH;
                l = array[hc];
                while (l != nullptr) {
                    if (l->value == a) {
                        return true;
                    }
                    else if(l->next != nullptr) {
                        l = l->next;
                    }
                    else { return false; }
                }
            }
            else {
                return false;
            }

        }

        catch (int i) {

            std::cout << "Error : " << i << "\n";

        }
    }

    void doEmpty() {
        for (int i = 0; i < size; i++) {
            if (array[i] != nullptr) {
                ListH* l = new ListH;
                ListH* l1 = new ListH;
                l = array[i];
                while (l->next != nullptr) {
                    l1 = l->next;
                    l = nullptr;
                    l = l1;
                }
                array[i] = nullptr;
            }

        }

    }

    bool checkEmpty() {
        for (int i = 0; i < size; i++) {
            if (array[i] != nullptr) {
                return false;
            }
        }    
        return true;
    }

    
    
};
