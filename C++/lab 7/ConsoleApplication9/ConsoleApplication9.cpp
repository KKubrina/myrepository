﻿// ConsoleApplication9.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include "Header.h"

int main()
{
    Tree* tree = new Tree();

    tree->insertElem("abc");
    tree->insertElem("cc");
    tree->insertElem("cc");
    tree->insertElem("bbc");
    tree->insertElem("db");
    tree->insertElem("adc");
    std::cout << "tree: " << *tree << "\n\n";


    Tree* tree1 = new Tree();
    tree1->operator=(*tree);
    Tree* tree2 = new Tree();
    tree2 = tree;

    
    std::cout << "amount of entries of word 'cc': " << tree->findElemSchet("cc") << "\n\n";
    std::cout << "amount of entriesof word 'bbc': " << tree->findElemSchet("bbc") << "\n\n";
    std::cout << "amount of entriesof word 'bbca': " << tree->findElemSchet("bbca") << "\n\n";

    tree->deleteElement("db");
    tree->deleteElement("dbd"); //error
    std::cout << "tree: " << *tree << "\n\n";
    std::cout << "amont or words of tree: " << tree->amount() << "\n\n";

    tree->deleteElement("cc");
    tree->deleteElement("cc");
    std::cout << "amont or words of tree: " << tree->amount() << "\n\n";

    std::cout << "tree: " << *tree << "\n\n";
    std::cout << "tree1: " << *tree1 << "\n\n";
    std::cout << "tree2: " << *tree2 << "\n\n";


}