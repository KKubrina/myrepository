#pragma once
#include <iostream>
#include <cstring>

typedef const char* TElem;


struct TreeElem // ������� ��������� ������
{
    TElem info;
    int schet;
    TreeElem* left;
    TreeElem* right;

    TreeElem(TElem a) {
        info = a;
        schet = 1;
        left = nullptr;
        right = nullptr;
    }

    TreeElem() {
    }

};

void printOut(std::ostream& out, TreeElem* root) {

    if (!root) { return; }
    printOut(out, root->left);
    out << "Word: " << root->info << " , ";
    out <<  "amount of entries: " << root->schet << "\n";
    printOut(out, root->right);

}

bool insertElem1(TreeElem*& root, TElem x)
{
    if (!root) {
        root = new TreeElem(x);
        return true;
    }
    if (strcmp(x, root->info) < 0) {
        return insertElem1(root->left, x);

    }
    if (strcmp(x, root->info) > 0) {
        return insertElem1(root->right, x);
    }
    root->schet += 1;
    return false;
}

int findElemSchet1(TreeElem* root, TElem x)
{
    if (!root) {
        return 0;
    }
    if (strcmp(x, root->info) < 0) {
        return findElemSchet1(root->left, x);
    }
    if (strcmp(x, root->info) > 0) {
        return findElemSchet1(root->right, x);
    }
    return root->schet;
}

void del2(TreeElem*& leftTree, TreeElem*& root)
{
    if (leftTree->right) { del2(leftTree->right, root); }
    else
    {
        root->info = leftTree->info;
        root = leftTree;
        leftTree = leftTree->left;
    }
}

bool deleteElem1(TreeElem*& root, TElem x)
{
    if (!root) {
        return false; // x � ������ �� ������
    }
    // ����� �������� x
    if (strcmp(x, root->info) < 0) {
        return deleteElem1(root->left, x);
    }
    if (strcmp(x, root->info) > 0) {
        return deleteElem1(root->right, x);
    }
    // ���� ������ ����, �� x == root->info, �������� ����
    TreeElem* pDel = root;
    if (root->schet > 1) {
        root->schet -= 1;
    }
    else {
        if (!root->left) { // ��� ������� �����,
            root = root->right; // �������� ������� ������
            // ��� nullptr, ���� ��� ���
        }
        else if (!root->right) { // ������������ ������
            root = root->left;
        }
        else { // ��� �������
            del2(root->left, pDel);
        }
        delete pDel; // � ����� ������ pDel � ����,
         // ������� ���� ��������� �������
    }
    return true;
}

int amount1(TreeElem* root) {
    if (!root) { return 0; }
    return (root->schet + amount1(root->left) + amount1(root->right));
}

void clear(TreeElem*& root)
{
    if (!root) return;
    clear(root->left);
    clear(root->right);
    delete root;
    root = nullptr;
}

class Tree {
public:
    TreeElem* root;

    Tree()
    {
        root = new TreeElem;
        root = nullptr;

    };

    ~Tree() {
        clear(root->left);
        clear(root->right);
        delete root;
        root = nullptr;
    }

    Tree& operator=(const Tree& x)
    {
        if (&x == this) {
            return *this;
        }
        if (root != nullptr) {
            clear(root);
        }
        root = new TreeElem;
        root->info = x.root->info;
        root->schet = x.root->schet;
        vspom(x.root, root);
        return *this;
    }

    void vspom(TreeElem* root2, TreeElem* root) {
        if (root2->left == nullptr) {
            if (root2->right == nullptr) {
                root->right = nullptr;
                root->left = nullptr;
                return;
            }
            else {
                root->left = nullptr;
                TreeElem* root12 = new TreeElem;
                root12->info = root2->right->info;
                root12->schet = root2->right->schet;
                root->right = root12;
                vspom(root2->right, root12);
            }

        }
        else if (root2->right == nullptr) {
            if (root2->left == nullptr) {
                root->left = nullptr;
                root->right = nullptr;
                return;
            }
            else {
                root->right = nullptr;
                TreeElem* root11 = new TreeElem;
                root11->info = root2->left->info;
                root11->schet = root2->left->schet;
                root->left = root11;
                vspom(root2->left, root11);
            }

        }
        else {
            TreeElem* root11 = new TreeElem;
            TreeElem* root12 = new TreeElem;
            root11->info = root2->left->info;
            root11->schet = root2->left->schet;
            root12->info = root2->right->info;
            root12->schet = root2->right->schet;
            root->left = root11;
            root->right = root12;
            vspom(root2->left, root11);
            vspom(root2->right, root12);
        }

    }


    void deleteElement(TElem a) {
        try {
            if (deleteElem1(root, a)) {}
            else { throw - 1; }
        }
        catch (int i)
        {
            std::cout << "Error #" << i << "\n";
        }
    }

    int findElemSchet(TElem a) {
        try {
            if (root) {
                return findElemSchet1(root, a);
            }
            else { throw 0; }
        }
        catch (int i)
        {
            std::cout << "Error #" << i << "\n";
        }
    }

    bool insertElem(TElem a) {
        return insertElem1(root, a);
    }

    int amount() {
        try {
            if (root) {
                return amount1(root);
            }
            else { throw 0; }
        }
        catch (int i)
        {
            std::cout << "Error #" << i << "\n";
        }
    }

    friend std::ostream& operator<< (std::ostream& out, const Tree& a);


};

std::ostream& operator<<(std::ostream& out, const Tree& a)
{
    try {
        if (a.root != nullptr) {
            printOut(out, a.root);
            return out;
        }
        else {
            out << "Error\n";
            return out;
            throw "Empty tree";
        }
    }

    catch (const char* a)
    {
        std::cout << "Error message: " << a << "\n";
    }

}
