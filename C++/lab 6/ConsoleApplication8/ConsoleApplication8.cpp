﻿// ConsoleApplication8.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include "Tree.h"

int main()
{
    Tree* tree1 = new Tree(7);
    tree1->addElement(5, new int[1]{ 0 }, 1);
    tree1->addElement(4, new int[2]{ 0, 0 }, 2);
    tree1->addElement(6, new int[2]{ 0, 1 }, 2);
    tree1->addElement(9, new int[1]{ 1 }, 1);
    tree1->addElement(8, new int[2]{ 1, 0 }, 2);
    tree1->addElement(10, new int[2]{ 1, 1 }, 2);
    Tree* tree2 = new Tree();
    Tree* tree3 = new Tree();
    tree2->operator=(*tree1);
    tree3 = tree1;
    std::cout << "tree1 : " << *tree1 << "\n\n";
    std::cout << "even: " << tree1->amountOfEven() << "\n\n";
    std::cout << "positive: " << tree1->positive() << "\n\n";
    std::cout << "arithmetic mean : " << tree1->srArifmetic() << "\n\n";
    std::vector<int>  a = tree1->seachElement(8);
    if (a.size() > 0) {
        std::cout << "Way from number (8): ";
        for (int i = 0; i < a.size(); i++) {
            std::cout << a[i] ;
        }
    }
    std::cout << "\n\n";


    tree1->deleteLeafsOfTree();
    std::cout << "tree1 : " << *tree1 << "\n\n";
    tree1->deleteLeafsOfTree();
    std::cout << "tree1 : " << *tree1 << "\n\n";
    std::cout << "tree2 : " << *tree2 << "\n\n";
    std::cout << "tree3 : " << *tree3 << "\n\n";
    //tree1->deleteLeafsOfTree();
    //std::cout << "tree1 : " << *tree1 << "\n\n";  //error

}

