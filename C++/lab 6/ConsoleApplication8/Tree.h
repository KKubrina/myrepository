#pragma once
#include<iostream> 
#include <vector>

typedef int TElem;



struct TreeElem // ������� ��������� ������
{
    TElem info;
    TreeElem* left;
    TreeElem* right;

};

void clear(TreeElem*& root)
{
    if (!root) return;
    clear(root->left);
    clear(root->right);
    delete root;
    root = nullptr;
}


void printOut(std::ostream& out, TreeElem* root) {

    if (!root) { return; }
    printOut(out, root->left);
    out << root->info << " ";
    printOut(out, root->right);

}


void deleteLeaf(TreeElem*& root) {
    if (root == nullptr) { return; }
    if (root->left == nullptr && root->right == nullptr) {
        clear(root);

    }
    else {
        deleteLeaf(root->left);
        deleteLeaf(root->right);
    }
}


int amountOfEven1(TreeElem* root) {
    if (!root) { return 0; }
    if (root->info % 2 == 0) {
        return (1 + amountOfEven1(root->left) + amountOfEven1(root->right));
    }
    else { return (amountOfEven1(root->left) + amountOfEven1(root->right)); }
}

int summa(TreeElem* root) {
    if (!root) { return 0; }
    return (root->info + summa(root->left) + summa(root->right));
}

int amount(TreeElem* root) {
    if (!root) { return 0; }
    return (1 + amount(root->left) + amount(root->right));
}


std::vector<int> seachElement1(TreeElem* root, TElem a, int& flag) {
    std::vector<int> answer;
    if (root == nullptr) { return answer; }
    if (root->info == a) {
        flag = 1;
        return answer;
    }
    else {
        answer.push_back(0);
        std::vector<int> answer1 = seachElement1(root->left, a, flag);
        for (int i = 0; i < answer1.size();i++) {
            answer.push_back(answer1[i]);
        };
        if (flag == 1) { return answer; }
        answer.pop_back();
        answer.push_back(1);
        std::vector<int> answer2 = seachElement1(root->right, a, flag);
        for (int i = 0; i < answer2.size();i++) {
            answer.push_back(answer2[i]);
        };
        if (flag == 1) { return answer; }
        answer.pop_back();
    }


}

bool positive1(TreeElem* root) {
    if (!root) { return true; }
    if (root->info < 0) {
        return false;
    }
    else { return (positive1(root->left) && positive1(root->right)); }
}



class Tree {
public:
    TreeElem* root;

    Tree(TElem theInfo)
    {
        root = new TreeElem;
        root->info = theInfo;
        root->left = nullptr;
        root->right = nullptr;

    };

    Tree()
    {
        root = new TreeElem;
        root = nullptr;

    };

    Tree& operator=(const Tree& x)
    {
        if (&x == this) {
            return *this;
        }
        if (root != nullptr) {
            clear(root);
        }
        root = new TreeElem;
        root->info = x.root->info;
        vspom(x.root, root);
        return *this;
    }

    void vspom(TreeElem* root2, TreeElem* root) {
        if (root2->left == nullptr) {
            if (root2->right == nullptr) {
                root->right = nullptr;
            root->left = nullptr;
            return;}
            else {
                root->left = nullptr;
                TreeElem* root12 = new TreeElem;
                root12->info = root2->right->info;
                root->right = root12;
                vspom(root2->right, root12);
            }
            
        }
        else if (root2->right == nullptr) {
            if (root2->left == nullptr) {
                root->left = nullptr;
            root->right = nullptr;
            return;}
            else {
                root->right = nullptr;
                TreeElem* root11 = new TreeElem;
                root11->info = root2->left->info;
                root->left = root11;
                vspom(root2->left, root11);
            }
            
        }
        else {
            TreeElem* root11 = new TreeElem;
            TreeElem* root12 = new TreeElem;
            root11->info = root2->left->info;
            root12->info = root2->right->info;
            root->left = root11;
            root->right = root12;
            vspom(root2->left, root11);
            vspom(root2->right, root12);
        }
    
    }

    Tree& operator=(Tree&& x) {

        vspom1(x.root->left);
        vspom1(x.root->right);
        std::swap(root, x.root);
        clear(x.root);
        return *this;
    }

    void vspom1(TreeElem* root2) {
        if (root2 == nullptr) {
            TreeElem* root1 = new TreeElem;
            std::swap(root1, root2);
            return;
        }
        else {
            TreeElem* root1 = new TreeElem;
            std::swap(root1, root2);
            vspom1(root2->left);
            vspom1(root2->right);
        }

    }

    ~Tree() {
        clear(root->left);
        clear(root->right);
        delete root;
        root = nullptr;
    }
    std::vector<int> seachElement(TElem a) {

        try {
            if (root) {
                int flag = 0;
                std::vector<int> v1 = seachElement1(root, a, flag);
                if (flag == 0) {
                    throw "This number was not found in the tree"; //������� �� ������ 
                }
                else { return v1; }
            }
        
            else { throw - 1; }
        }
         catch (int i)
    {
        std::cout << "Error #" << i << "\n";
    }
         catch (const char* a)
         {
             std::cout << "Error message: " << a << "\n";
         }
        
    }

    int amountOfEven() {
        try {
            if (root) {
                return amountOfEven1(root);
            }
            else { throw 0; }
        }
        catch (int i)
        {
            std::cout << "Error #" << i << "\n";
        }
    }

    double srArifmetic() {
        try {
            if (root) {
                if (root == nullptr) { return 0; }
                double a = amount(root);
                double b = summa(root);
                return b / a;
            }
            else { throw 0; }
        }

        catch (int i)
        {
            std::cout << "Error #" << i << "\n";
        }
    }

    bool positive() {
        try {
            if (root) {
                return positive1(root);
            }
            else { throw 0; }
        }

        catch (int i)
        {
            std::cout << "Error #" << i << "\n";
        }
    }

    void addElement(TElem theInfo, int* a, int sizea) {
        try {
            if (root) {
                TreeElem* nowElement = root;
                TreeElem* nowElement1 = new TreeElem();
                int flag = -1;
                for (int i = 0; i < sizea; i++) {
                    if (nowElement != nullptr) {
                        if (a[i] == 0) {
                            nowElement1 = nowElement;
                            nowElement = nowElement->left;
                            flag = 0;
                        }
                        else if (a[i] == 1) {
                            nowElement1 = nowElement;
                            nowElement = nowElement->right;
                            flag = 1;
                        }
                        else {
                            throw 0;
                        }
                    }
                    else { throw 1; }
                }
                if (nowElement == nullptr) {
                    nowElement = new TreeElem;
                    nowElement->info = theInfo;
                    nowElement->left = nullptr;
                    nowElement->right = nullptr;
                    if (flag == 0) {
                        nowElement1->left = nowElement;
                    }
                    else { nowElement1->right = nowElement; }
                }
                else {
                    nowElement->info = theInfo;
                }
            }
            else { throw 0; }
        }
        catch (int i)
        {
            std::cout << "Error #" << i << "\n";
        }
    }

    void deleteLeafsOfTree() {
        try {
            if (root) {
                deleteLeaf(root);
            }
            else { throw 0; }
        }

        catch (int i)
        {
            std::cout << "Error #" << i << "\n";
        }
    }

    friend std::ostream& operator<< (std::ostream& out, const Tree& a);
};


std::ostream& operator<<(std::ostream& out, const Tree& a)
{
    try {
        if (a.root != nullptr) {
            printOut(out, a.root);
            return out;
        }
        else { 
            out << "Error\n";
            return out;
            throw "Empty tree"; }
    }

    catch (const char* a)
    {
        std::cout << "Error message: " << a << "\n";
    }

}

