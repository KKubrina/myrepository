﻿// ConsoleApplication3.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//


#include <iostream>
#include "Box.h"
#include "Container.h"

int main()
{
	
	ConBox::Box box01 = ConBox::Box(1, 1, 1, 1, 1);
	ConBox::Box box02 = ConBox::Box(2, 2, 2, 2, 2);
	ConBox::Box box03 = ConBox::Box(3, 3, 3, 3, 3);

	ConBox::Container container = ConBox::Container(10000, 11000, 12000, 2);
	container.addBox(0, box01);
	container.addBox(1, box03);
	container.addBox(2, box02);
	std::cout << "Amount of box " << container.amountOfBox() << '\n';
	std::cout << "Weight all boxes " << container.weightAll() << '\n';
	std::cout << "Value all boxes " << container.valueAll() << '\n';

	container.deleteBox(1);
    int b = container.addBoxWithPam(box03);
	std::cout << container.getBox(1).getValue() << '\n';
	container[1] = ConBox::Box(1, 1, 1, 1, 4);
	std::cout << container.getBox(1).getValue() << '\n';

	return 0;
}


