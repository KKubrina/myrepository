﻿// ConsoleApplication11.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
#define _USE_MATH_DEFINES

#include <iostream>
#include <math.h>

double enterDouble(double* x) {
    char fmt[] = "%lf";
    __asm {
        mov ebx, x
        lea eax, [ebx]
        push eax
        lea eax, fmt
        push eax
        call scanf
        add esp, 8
    }
}

void showDouble(const double x) {
    char fmt[] = "%lf";
    __asm {
        push dword ptr[x + 4]
        push dword ptr x
        lea eax, fmt
        push eax
        call printf
        add esp, 12
    }
}



double function(const double x, double epsilon = 1e-5) {
    double answer;
    double pi_2 = M_PI*2;

    //Σ((-1)^n * x^(2n+1))/(2n+1)! - ряд Тейлора для sin(x)

    int i = 0;
    __asm {
        finit
        fldz   // сумма ряда
        lea esi, i   // n
        

        fld qword ptr x
        fld qword ptr pi_2
        fcomi st(0), st(1) //проверяем что х меньше периода
        ja BEFORE_CYCLE
        fld qword ptr x //если нет, то заходим сюда
        

        fdiv st(0), st(1) //делим х на 2п
        frndint //берем целую часть от получившегося числа
        fmul st(0), st(1) //умножаем целую часть на 2п
        fsubp st(2), st(0) //вычитаем из начального х получившееся число

        BEFORE_CYCLE:
        fstp st(0)
        fstp x 
        

        CYCLE :

        fld1
            fchs
            mov eax, [esi]
            mov edx, 0
            mov ebx, 2
            div ebx
            cmp edx, 0
            jne NEXT
            fabs   // в st(0) (-1)^n

            NEXT :

        mov eax, 2
            mul[esi]
            xor edi, edi
            fld qword ptr x
            fld st(0)
            FOR:
        cmp edi, eax
            jge NEXT1
            fmul st(1), st(0)
            inc edi
            jmp FOR


            NEXT1 :
        fstp st(0)   // в st(0) x^(2n + 1), в st(1) (-1)^n
            fmulp st(1), st(0)
            fild dword ptr [esi]
            fld1
            fadd st(0), st(0)
            fmulp st(1), st(0)
            fld1
            faddp st(1), st(0)
            fld st(0)

            WHILE:
        fld1
            fcomip st(0), st(1)
            je NEXT2
            fld1
            fsubp st(1), st(0)
            fmul st(1), st(0)
            jmp WHILE

            NEXT2 :
        fstp st(0)   // в st(0) (2n + 1)!
            fdivp st(1), st(0)
            fadd st(1), st(0)   // сумма ряда в st(1)
            fabs   
            fld qword ptr epsilon
            fcomip st(0), st(1)
            ja FIN
            fstp st(0)
            fst answer
            inc [esi]
            jmp CYCLE


            FIN :
        fstp st(0)
            fstp answer
    }
    return answer;
}


void enterInt(int* a) {

    char fmt[] = "%d";
    __asm {
        mov ebx, a
        lea eax, [ebx]
        push eax
        lea eax, fmt
        push eax
        call dword ptr scanf
        add esp, 8

    }
}


const void showStr(const char* line, int sizeLine) {
    char fmt[] = "%c";
    __asm {
        mov ebx, line
        mov ecx, sizeLine

        cmp ecx, 0
        je FIN

        CYCLE :

        push ebx
        push ecx
        mov eax, [ebx]
        push eax
        lea eax, fmt
        push eax
        call printf
        add esp, 8
        pop ecx
        pop ebx
        inc ebx
        loop CYCLE

        FIN :
    }
}


int main()
{ //0.523598775598298(pi/6) = 0.5
  //-0.523598775598298(-pi/6) = -0.5
  //0 = 0

    double x;
    double answer;
    int end;

    do {
        showStr("Enter the param x of sin(x): ", 30);
        enterDouble(&x);
        showStr("Value of function = ", 21);
        answer = function(x);
        showDouble(answer);
        showStr("\nPress 1 to continue or 0 to exit: ", 37);
        enterInt(&end);
    } while (end == 1);
}


